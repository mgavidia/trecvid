TRECVID Project
===============

TRECVID project

Requirements
============

* Requires java jdk/jre runtime 1.7 (Dependency JFeatureLib code.google.com/p/jfeaturelib)
* ant or maven (preferred)

How To Build
============

* Have the java runtime jre and jdk installed release 1.7 or higher
* Install [Apache Ant](http://ant.apache.org/) or [Apache Maven](https://maven.apache.org/)
* run ant or mvn package in the root directory
* For ant build, to Generate all debugging run ant -Ddebug=true

How to Run
==========
* ./run.sh
* or
* To run directly:
    - if built with ant: java -cp bin/trecvid-1.0.jar edu.miami.trecvid.Main
    - or with maven: java -cp target/trecvid-1.0-jar-with-dependencies.jar edu.miami.trecvid.Main