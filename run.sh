#!/bin/bash

exec="java -cp"
jarname="trecvid-1.0"
jar="bin/${jarname}.jar"
main="edu.miami.trecvid.Main"

if [ ! -e "$jar" ]; then
    jar="target/${jarname}-jar-with-dependencies.jar"
fi

if [ -e "$jar" ]; then
    $exec $jar $main "$@"
else
    echo "You need to build first. Run ant to create the basic build."
    echo "To include the hadoop classes in the build, use mvn package"
fi
