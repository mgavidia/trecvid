#!/bin/bash

MAIN_DIR=/trecvid/2015

LABELS=$MAIN_DIR/labels

TRAINING_DIR=$MAIN_DIR/classifiers/svm/training
TESTING_DIR=$MAIN_DIR/classifiers/svm/testing

# Do training
hadoop jar target/trecvid-1.0-jar-with-dependencies.jar edu.miami.trecvid.classifiers.svm.SVM $TRAINING_DIR $TESTING_DIR $LABELS
