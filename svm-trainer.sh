#!/bin/bash

if [ $# -eq 0 ]; then
    echo "usage: $0 <training-input> <testing-input> <label-directory> <output-directory> <split>"
    exit 0
fi

# Errors
set -e

TRAINING_FILE=$1
TESTING_FILE=$2
LABEL_DIRECTORY=$3
OUTPUT_DIRECTORY=$4
SPLIT=$5
LINE_SIZE=`wc -l < $TRAINING_FILE`
BLOCK_SIZE=`expr $LINE_SIZE / $SPLIT`

mkdir -p $OUTPUT_DIRECTORY/temp
mkdir -p $OUTPUT_DIRECTORY/training
mkdir -p $OUTPUT_DIRECTORY/testing
split -l $BLOCK_SIZE -d $TRAINING_FILE $OUTPUT_DIRECTORY/temp/train_

for j in `ls -v $LABEL_DIRECTORY`; do
    LABEL_NUMBER="${j%.*}"
    CURRENT_NUMBER=100000

    TEMP_DIRECTORY=$OUTPUT_DIRECTORY/$LABEL_NUMBER

    # Create directory for label model
    mkdir -p $TEMP_DIRECTORY


    # Create first model
    ./run.sh classifiers:svm:train -l $LABEL_DIRECTORY/$j -i $OUTPUT_DIRECTORY/temp/train_00 -o $TEMP_DIRECTORY -p test -f train_${CURRENT_NUMBER:1}

    for i in `ls -v $OUTPUT_DIRECTORY/temp`; do
        if [ "$i" == "train_00" ]; then
            continue;
        fi
        LAST_NUMBER=$CURRENT_NUMBER
        CURRENT_NUMBER=`expr $CURRENT_NUMBER + 1`
        ./run.sh classifiers:svm:train -l $LABEL_DIRECTORY/$j -i $OUTPUT_DIRECTORY/temp/$i -o $TEMP_DIRECTORY -p test -f train_${CURRENT_NUMBER:1} -m $TEMP_DIRECTORY/test-train_${LAST_NUMBER:1}.model
    done
    MODEL_FILE="$OUTPUT_DIRECTORY/training/$LABEL_NUMBER.model"
    mv $TEMP_DIRECTORY/test-train_${CURRENT_NUMBER:1}.model $MODEL_FILE
    rm -fr $TEMP_DIRECTORY

    # Run the testing
    ./run.sh classifiers:svm:test -i $TESTING_FILE -o $OUTPUT_DIRECTORY/testing -p test -c $LABEL_NUMBER -f rank -m $MODEL_FILE
done

rm -fr $OUTPUT_DIRECTORY/temp


