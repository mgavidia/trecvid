package edu.miami.trecvid.classifiers;

import java.io.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.NumberFormatException;

import edu.miami.trecvid.utilities.*;

public class LabelValue {

    private class Key {
        public int videoid;
        public int shotid;

        public Key(){
            this(0,0);
        }

        public Key(int v, int s){
            this.videoid = v;
            this.shotid = s;
        }

        public Key(String v, String s){
            videoid = Integer.parseInt(v);
            shotid = Integer.parseInt(s);
        }

        public int compareTo(Key key){
            int video = Integer.compare(videoid, key.videoid);
            if (video == 0){
                return Integer.compare(shotid, key.shotid);
            }
            return video;
        }

        @Override
        public boolean equals(Object o){
            if (this == o) return true;
            if (!(o instanceof Key)) return false;
            Key key = (Key) o;
            return this.videoid == key.videoid && this.shotid == key.shotid;
        }

        @Override
        public int hashCode(){
            return videoid * 31 + shotid;
        }
    }

    private Map<Key, Integer> labels = new HashMap<Key, Integer>();

    public LabelValue(String filename) throws IOException, FileNotFoundException{
        final int totalLines = Tools.countLines(filename);
        BufferedReader br= new BufferedReader(new InputStreamReader(new FileInputStream(new File(filename))));
        String line;
        line=br.readLine();
        int count=0;
        while (line != null){
            List<String> values = new LinkedList<String>(Arrays.asList(line.split(",")));
            String videoid = values.get(0);
            String shotid = values.get(1);
            Integer label = Integer.decode(values.get(2));
            Tools.printCompletionRate(count++, totalLines, String.format("Mapping videoid [%s] & shotid [%s] to label [%d]", videoid, shotid, label));
            labels.put(new Key(videoid, shotid), label);
            line=br.readLine();
        }
        System.out.println();
    }

    public int get(int videoid, int shotid){
        Key key = new Key(videoid, shotid);
        Integer value = labels.get(key);
        return value.intValue();
    }

    public int get(Double videoid, Double shotid){
        return get(videoid.intValue(), shotid.intValue());
    }

    public String get(String videoid, String shotid){
        return String.valueOf(get(Integer.parseInt(videoid), Integer.parseInt(shotid)));
    }
}