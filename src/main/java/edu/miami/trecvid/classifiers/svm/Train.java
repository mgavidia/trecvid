package edu.miami.trecvid.classifiers.svm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.NumberFormatException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.cli.*;

import libsvm.*;

import edu.miami.trecvid.classifiers.LabelValue;
import edu.miami.trecvid.utilities.*;

public class Train implements CallInterface {

    private final static String COMMAND_NAME = "classifiers:svm:train";
    private static svm_parameter parameter = null;
    private static String outDirectory = null;
    private static String prefix = "";
    private static String defaultName = "training";
    private static LabelValue labels = null;

    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        try {
            CommandLineParser parser = new DefaultParser();
            Options options = new Options();
            options.addOption("h", "help", false, "Show this help menu");
            options.addOption(Option.builder("i")
                    .longOpt("inputfile")
                    .argName("file")
                    .hasArg()
                    .desc("Feature Input File")
                    .build());
            options.addOption(Option.builder("o")
                    .longOpt("output")
                    .argName("directory")
                    .hasArg()
                    .desc("Output directory")
                    .build());
            options.addOption(Option.builder("p")
                    .longOpt("prefix")
                    .argName("name")
                    .hasArg()
                    .desc("Prefix to model output name")
                    .build());
            options.addOption(Option.builder("f")
                    .longOpt("filename")
                    .argName("name")
                    .hasArg()
                    .desc("Name of model output, defaults to 'training'")
                    .build());
            options.addOption(Option.builder("s")
                    .longOpt("split")
                    .argName("number")
                    .hasArg()
                    .desc("Split file: i.e. lines/number")
                    .build());
            options.addOption(Option.builder("m")
                    .longOpt("model")
                    .argName("file")
                    .hasArg()
                    .desc("Train input with given model file")
                    .build());
            options.addOption(Option.builder("l")
                    .longOpt("labels")
                    .argName("file")
                    .hasArg()
                    .desc("Label file")
                    .build());
            CommandLine line = parser.parse(options, args);
            if (line.getOptions().length == 0 || line.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp(COMMAND_NAME, options);
                return;
            }

            if (line.hasOption('p')){
                prefix = line.getOptionValue("p");
                System.out.println("    Prefix set to: [" + prefix + "]");
            }

            if (line.hasOption('f')){
                defaultName = line.getOptionValue("f");
                System.out.println("    Model name set to: [" + defaultName + "]");
            }

            if (!line.hasOption("l")){
                throw new ParseException("Missing argument 'l'");
            }
            labels = new LabelValue(line.getOptionValue("l"));

            if (line.hasOption('i') && line.hasOption('o') && line.hasOption('m')) {
                trainWithModel(line.getOptionValue('i'), line.getOptionValue('o'), line.getOptionValue('m'));
            } else if (line.hasOption('i') && line.hasOption('o') && line.hasOption('s')){
                splitTraining(line.getOptionValue('i'), line.getOptionValue('o'), line.getOptionValue('s'));
            } else if (line.hasOption('i') && line.hasOption('o')){
                train(line.getOptionValue('i'), line.getOptionValue('o'));
            }

        } catch (ParseException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }

    // Get Description
    public String description(){
        return "Train and output the model with SVM classifier.";
    }

    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList(COMMAND_NAME));
    }

    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("-h or --help to display argument options"));
    }

    private static double[][] convertFromList(List<double[]> list, boolean out){
        double [][] newlist = new double[list.size()][];
        for (int i = 0; i < list.size(); i++){
            if (out){
                Tools.printCompletionRate(i, list.size(), "Converting list");
            }
            newlist[i] = list.get(i);
        }
        return newlist;
    }

    private static String getName(){
        return getName(defaultName);
    }

    private static String getName(int index){
        return getName(defaultName, index);
    }

    private static String getName(String name){
        StringBuilder sb = new StringBuilder();
        if (prefix.length() > 0){
            sb.append(prefix);
            sb.append("-");
        }
        sb.append(name);
        sb.append(".model");
        return sb.toString();
    }

    private static String getName(String name, int index){
        final String suffix = String.valueOf(index);
        return getName(name + "-" + suffix);
    }

    public static svm_model testNext(String outdir, List<double[]> map, svm_model model, String testModelName) throws IOException{
        List<double[]> newMap = new ArrayList<double[]>();
        int count = 1;
        for (double[] vector : map){
            Tools.printCompletionRate(count, map.size(), String.format("Testing vector [%d of %d].", (count++) - 1, map.size()));
            double[] videoInfo = new double[] {vector[0], vector[1]};
            double[] values = Test.getProbabilities(vector, model);
            values = ArrayUtils.addAll(videoInfo, values);
            newMap.add(values);
        }
        return createModel(outdir, testModelName, newMap);
    }

    public static svm_model createModel(String outdir, String filename, List<double[]> map) throws IOException{
        double [][] train = convertFromList(map, true);

        System.out.println();

        svm_problem prob = new svm_problem();
        int dataCount = train.length;
        prob.y = new double[dataCount];
        prob.l = dataCount;
        prob.x = new svm_node[dataCount][];

        for (int i = 0; i < dataCount; i++){
            Tools.printCompletionRate(i, dataCount, String.format("Setting up model parameters [%d of %d].", i+1, dataCount));
            double[] features = train[i];
            prob.x[i] = new svm_node[features.length-2];
            for (int j = 2; j < features.length; j++){
                svm_node node = new svm_node();
                node.index = j;
                node.value = features[j];
                prob.x[i][j-2] = node;
            }
            prob.y[i] = labels.get((int)features[0], (int)features[1]);
        }

        System.out.println();
        System.out.println("Creating model... (this may take a while).");
        svm_model model = svm.svm_train(prob, parameter);
        svm.svm_save_model(outdir + "/" + filename, model);
        System.out.println();
        System.out.println("Built model file " + filename);

        return model;
    }

    private static void processInput(int totalLines, int split, InputStream in){
        try {
            List<double[]> map = new ArrayList<double[]>();
            BufferedReader br= new BufferedReader(new InputStreamReader(in));
            String line;
            line=br.readLine();
            int splitTotal = totalLines / split;
            int count = 0;
            int currentSplit = 0;
            svm_model previousModel = null;
            while (line != null){
                List<String> list = new LinkedList<String>(Arrays.asList(line.split(",")));
                Integer videoid = Integer.decode(list.get(0)); //list.remove(0));
                Integer shotid = Integer.decode(list.get(1)); //list.remove(0));
                if (count >= splitTotal){
                    System.out.println();
                    count = 0;
                    // first set
                    if (previousModel == null) {
                        previousModel = createModel(outDirectory, getName(currentSplit++), map);
                    } else {
                        // Test next set (update with newer model)
                        previousModel = testNext(outDirectory, map, previousModel, getName(currentSplit++));
                    }
                    map.clear();
                }
                Tools.printCompletionRate(count++, totalLines, String.format("Reading videoid [%d] shotid [%d]", videoid, shotid));
                double [] values = new double[list.size()];
                for (int i = 0; i < list.size(); i++){
                    values[i] = Double.valueOf(list.get(i));
                }
                map.add(values);
                line=br.readLine();
            }
            if (count != splitTotal) {
                //createModel(outDirectory, "training", currentSplit, map);
                testNext(outDirectory, map, previousModel, getName(currentSplit++));
            }

        } catch (final IOException ex){
        }
    }

    public static void init(){
        parameter = new svm_parameter();
        parameter.probability = 1;
        parameter.gamma = 0.5;
        parameter.nu = 0.5;
        parameter.C = 1;
        parameter.svm_type = svm_parameter.C_SVC;
        parameter.kernel_type = svm_parameter.LINEAR;
        parameter.cache_size = 20000;
        parameter.eps = 0.001;

        svm.svm_set_print_string_function(new svm_print_interface() {
            private int advance = 0;
            private static final int nextPrint = 1000;
            private int current = 1;
            private boolean forward = true;
            private static final String marker = "[-----------------------------------------------------------------]";

            //private static final String marker = "[=================================================================]";
            public void print(String str) {
                /*if (advance == 0){
                    System.out.print(marker.substring(0,current-1) + "#" + marker.substring(current+1) + "\r");
                }
                if (forward && ++advance > nextPrint){
                    advance = 0;
                    if (++current == 66){
                        forward = false;
                    }
                } else if (++advance > nextPrint){
                    advance = 0;
                    if (--current == 1){
                        forward = true;
                    }
                }*/
            }
        });
    }

    private static void train(String input, String outdir) throws IOException {
        System.out.println("Performing training of normal model.");
        init();
        outDirectory = outdir;
        List<double[]> map = FileTools.listFromInput(input);
        createModel(outDirectory, getName(), map);
    }

    private static void trainWithModel(String input, String outdir, String modelfile) throws IOException {
        System.out.println("Performing training with an existing input model.");
        init();
        outDirectory = outdir;
        List<double[]> map = FileTools.listFromInput(input);
        System.out.println("Loading model file: " + modelfile);
        System.out.println();
        svm_model model = svm.svm_load_model(modelfile);
        System.out.println("Training model...");
        testNext(outdir, map, model, getName());
    }

    private static void splitTraining(String input, String outdir, String count) throws IOException{
        final int split = Integer.parseInt(count);
        final int totalLines = Tools.countLines(input);
        System.out.println(String.format("Performing full training with [%d] splits.", split));

        init();
        outDirectory = outdir;
        // In case existing content
        File dir = new File(outDirectory);
        Tools.deleteDirectory(dir);
        dir.mkdir();
        processInput(totalLines, split, FileTools.getStream(input));
        return;
    }

}
