package edu.miami.trecvid.classifiers.svm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

import edu.miami.trecvid.utilities.*;

public class FileTools {
    public static InputStream getStream(String file) throws IOException{
        return new FileInputStream(new File(file));
    }

    public static List<double[]> listFromInput(String input) throws IOException {
        final int totalLines = Tools.countLines(input);
        List<double[]> map = new ArrayList<double[]>();
        BufferedReader br= new BufferedReader(new InputStreamReader(getStream(input)));
        String line;
        line=br.readLine();
        int count=0;
        while (line != null){
            List<String> list = new LinkedList<String>(Arrays.asList(line.split(",")));
            Integer videoid = Integer.decode(list.get(0));
            Integer shotid = Integer.decode(list.get(1));
            Tools.printCompletionRate(count++, totalLines, String.format("Reading videoid [%d] shotid [%d]", videoid, shotid));
            double [] values = new double[list.size()];
            for (int i = 0; i < list.size(); i++){
                values[i] = Double.valueOf(list.get(i));
            }
            map.add(values);
            line=br.readLine();
        }
        return map;
    }
}