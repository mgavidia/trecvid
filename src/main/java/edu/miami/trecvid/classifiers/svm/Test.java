package edu.miami.trecvid.classifiers.svm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.NumberFormatException;
import java.lang.Double;

import java.math.BigDecimal;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.cli.*;

import libsvm.*;

import edu.miami.trecvid.utilities.*;

public class Test implements CallInterface {

    private final static String COMMAND_NAME = "classifiers:svm:test";
    private static String outDirectory = null;
    private static String prefix = "test";
    private static String concept = null;
    private static String defaultName = "rank";

    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        try {
            CommandLineParser parser = new DefaultParser();
            Options options = new Options();
            options.addOption("h", "help", false, "Show this help menu");
            options.addOption(Option.builder("i")
                    .longOpt("inputfile")
                    .argName("file")
                    .hasArg()
                    .desc("Feature Test Input File")
                    .build());
            options.addOption(Option.builder("o")
                    .longOpt("output")
                    .argName("directory")
                    .hasArg()
                    .desc("Output directory")
                    .build());
            options.addOption(Option.builder("p")
                    .longOpt("prefix")
                    .argName("name")
                    .hasArg()
                    .desc("Prefix to score output name, defaults to 'test'")
                    .build());
            options.addOption(Option.builder("c")
                    .longOpt("concept")
                    .argName("name")
                    .hasArg()
                    .desc("Concept name, ie set as '10' and output would be: test_10_rank.txt")
                    .build());
            options.addOption(Option.builder("f")
                    .longOpt("filename")
                    .argName("name")
                    .hasArg()
                    .desc("Name of score output, defaults to 'rank'")
                    .build());
            options.addOption(Option.builder("m")
                    .longOpt("model")
                    .argName("file")
                    .hasArg()
                    .desc("Train input with given model file")
                    .build());
            CommandLine line = parser.parse(options, args);
            if (line.getOptions().length == 0 || line.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp(COMMAND_NAME, options);
                return;
            }

            if (line.hasOption('p')){
                prefix = line.getOptionValue("p");
                System.out.println("    Prefix set to: [" + prefix + "]");
            }

            if (line.hasOption('f')){
                defaultName = line.getOptionValue("f");
                System.out.println("    Model name set to: [" + defaultName + "]");
            }

            if (!line.hasOption('o')){
                throw new ParseException("Missing argument 'o' for output directory.");
            }

            if (!line.hasOption('c')){
                throw new ParseException("Missing argument 'c' for concept name.");
            }

            concept = line.getOptionValue('c');
            outDirectory = line.getOptionValue('o');

            runScores(line.getOptionValue('i'), line.getOptionValue('m'));

        } catch (ParseException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }

    // Get Description
    public String description(){
        return "Test with training model using SVM classifier.";
    }

    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList(COMMAND_NAME));
    }

    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("-h or --help to display argument options"));
    }

    private static class Results {
        public final double score;
        public final double [] probabilities;
        public Results(double score, double [] prob){
            this.score = score;
            this.probabilities = prob;
        }
    }

    private static Results evaluate(double[] features, svm_model model)
    {
        svm_node[] nodes = new svm_node[features.length-2];
        for (int i = 2; i < features.length; i++)
        {
            svm_node node = new svm_node();
            node.index = i;
            node.value = features[i];

            nodes[i-2] = node;
        }
        double v = -1;
        int totalClasses = model.label.length;
        int[] labels = new int[totalClasses];
        svm.svm_get_labels(model, labels);

        double[] prob_estimates = new double[totalClasses];
        v = svm.svm_predict_probability(model, nodes, prob_estimates);

        return new Results(v, prob_estimates);
    }

    public static double getScore(double[] features, svm_model model){
        return evaluate(features, model).score;
    }

    public static double [] getProbabilities(double[] features, svm_model model){
        return evaluate(features, model).probabilities;
    }

    private static String getFilename() {
        return outDirectory + "/" + prefix + "_" + concept + "_" + defaultName + ".txt";
    }

    public static void runScores(String testFile, String modelFile) throws IOException {
        System.out.println("Creating scores from testing file " + testFile + "...");
        List<double[]> map = FileTools.listFromInput(testFile);
        System.out.println("Loading model file: " + modelFile);
        System.out.println();
        svm_model model = svm.svm_load_model(modelFile);
        System.out.println("Testing model...");
        PrintWriter output = new PrintWriter(new FileWriter(getFilename()));
        int count = 1;
        for (double[] vector : map){
            Tools.printCompletionRate(count, map.size(), String.format("Testing vector [%d of %d].", count++, map.size()));
            double score = Test.getScore(vector, model);
            //output.println((new BigDecimal(score).toPlainString()) + ",0.0");
            output.println(String.format("%f,0.0",score));
        }
        output.close();
        System.out.println("Wrote scores to file: " + getFilename());
    }
}
