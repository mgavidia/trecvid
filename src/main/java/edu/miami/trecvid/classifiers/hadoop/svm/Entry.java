package edu.miami.trecvid.classifiers.hadoop.svm;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * A single entry on a given vector
 */
public class Entry implements Writable {

    // First
    private IntWritable shotid;
    private DoubleWritable value;


    public Entry() {
        this.shotid = new IntWritable();
        this.value = new DoubleWritable();
    }

    public Entry(IntWritable shotid, DoubleWritable value) {
        this.shotid = shotid;
        this.value = value;
    }

    public Entry(int shotid, double value) {
        this.shotid = new IntWritable(shotid);
        this.value = new DoubleWritable(value);
    }

    public void write(DataOutput dataOutput) throws IOException {
        shotid.write(dataOutput);
        value.write(dataOutput);
    }

    public void readFields(DataInput dataInput) throws IOException {
        shotid.readFields(dataInput);
        value.readFields(dataInput);
    }

    public IntWritable getShotId() {
        return shotid;
    }

    public DoubleWritable getValue() {
        return value;
    }

    public void setShotId(IntWritable id) {
        this.shotid = id;
    }

    public void setValue(DoubleWritable value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return shotid.hashCode();
    }
}