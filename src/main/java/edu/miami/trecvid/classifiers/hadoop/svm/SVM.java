package edu.miami.trecvid.classifiers.hadoop.svm;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Reducer;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.lang.StringBuilder;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Random;

import libsvm.*;

public class SVM {

    /*
    public class Map extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, Entry> {

        public void map(LongWritable key, Text value,
                        OutputCollector<IntWritable, Entry> output, Reporter reporter)
                throws IOException {
            String line = value.toString();

            List<String> list = new LinkedList<String>(Arrays.asList(line.split(",")));
            IntWritable videoid = new IntWritable(Integer.decode(list.remove(0)));
            IntWritable shotid = new IntWritable(Integer.decode(list.remove(0)));
            for (String item : list){
                Entry entry = new Entry(shotid, new DoubleWritable(Double.valueOf(item)));
                output.collect(videoid, entry);
            }
        }
    }
    public class Reduce extends MapReduceBase implements
            Reducer<IntWritable, Entry, Text, Text> {
        public void reduce(IntWritable key, Iterator<Entry> values,
                           OutputCollector<Text, Text> output, Reporter reporter)
                throws IOException {

            StringBuilder builder = new StringBuilder();
            Integer shotid = -1;
            while (values.hasNext()){
                Entry entry = values.next();
                if (shotid == -1){
                    shotid = entry.getShotId().get();
                    builder.append(shotid.toString() + ",");
                }
                Double value = entry.getValue().get();
                if (value.doubleValue() == value.longValue()) {
                    builder.append("," + String.valueOf(value.intValue()));
                } else {
                    builder.append("," + value.toString());
                }
            }
            output.collect(new Text(String.valueOf(key.get())), new Text(builder.toString()));
        }
    }*/

    public static class FeatureKey implements WritableComparable<FeatureKey> {
        private Integer videoid;
        private Integer shotid;

        public FeatureKey(){
            this(0,0);
        }

        public FeatureKey(String values){
            String [] split = values.split(",");
            videoid = Integer.parseInt(split[0]);
            shotid = Integer.parseInt(split[1]);
        }

        public FeatureKey(int videoid, int shotid){
            this.videoid = videoid;
            this.shotid = shotid;
        }

        public void write(DataOutput out) throws IOException {
            out.writeInt(videoid);
            out.writeInt(shotid);
        }

        public void readFields(DataInput in) throws IOException {
            videoid = in.readInt();
            shotid = in.readInt();
        }

        public String toString() {
            return String.valueOf(videoid) + "," + String.valueOf(shotid);
        }

        @Override
        public int compareTo(FeatureKey key){
            int video = this.videoid.compareTo(key.videoid);
            if (video == 0){
                return this.shotid.compareTo(key.shotid);
            }
            return video;
        }

        public boolean equals(FeatureKey key){
            return this.videoid.equals(key.videoid) && this.shotid.equals(key.shotid);
        }

        public int hashCode(){
            return videoid * 31 + shotid;
        }
    }
    
    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, FeatureKey, Text> {

        private final Pattern pattern = Pattern.compile("(^\\d+,\\d+),(.*$)");

        public void map(LongWritable key, Text value, OutputCollector<FeatureKey, Text> output, Reporter reporter) throws IOException {
            //String line = value.toString();
            Matcher matches = pattern.matcher(value.toString());
            while (matches.find()){
                output.collect(new FeatureKey(matches.group(1)), new Text(matches.group(2)));
            }
        }
    }

    public static class Reduce extends MapReduceBase implements
            Reducer<FeatureKey, Text, FeatureKey, Text> {
        public void reduce(FeatureKey key, Iterator<Text> values, OutputCollector<FeatureKey, Text> output, Reporter reporter) throws IOException {
            StringBuilder builder = new StringBuilder();
            boolean first = false;
            while (values.hasNext()){
                String entry = values.next().toString();
                if (entry.isEmpty()){
                    continue;
                }
                if (!first){
                    builder.append(entry.substring(1));
                    first = true;
                } else {
                    builder.append(entry.substring(0,1).equals(",") ? entry : "," + entry);
                }
            }
            output.collect(key, new Text(builder.toString()));
        }
    }

    private static svm_model train(double [][] train){ 
        svm_problem prob = new svm_problem();
        int dataCount = train.length;
        prob.y = new double[dataCount];
        prob.l = dataCount;
        prob.x = new svm_node[dataCount][];     

        for (int i = 0; i < dataCount; i++){            
            double[] features = train[i];
            prob.x[i] = new svm_node[features.length-1];
            for (int j = 1; j < features.length; j++){
                svm_node node = new svm_node();
                node.index = j;
                node.value = features[j];
                prob.x[i][j-1] = node;
            }           
            prob.y[i] = features[0];
        }               

        svm_parameter param = new svm_parameter();
        param.probability = 1;
        param.gamma = 0.5;
        param.nu = 0.5;
        param.C = 1;
        param.svm_type = svm_parameter.C_SVC;
        param.kernel_type = svm_parameter.LINEAR;       
        param.cache_size = 20000;
        param.eps = 0.001;      

        svm_model model = svm.svm_train(prob, param);

        return model;
    }

    private static double evaluate(double[] features, svm_model model) 
    {
        svm_node[] nodes = new svm_node[features.length-1];
        for (int i = 1; i < features.length; i++)
        {
            svm_node node = new svm_node();
            node.index = i;
            node.value = features[i];

            nodes[i-1] = node;
        }

        int totalClasses = 2;       
        int[] labels = new int[totalClasses];
        svm.svm_get_labels(model,labels);

        double[] prob_estimates = new double[totalClasses];
        double v = svm.svm_predict_probability(model, nodes, prob_estimates);

        for (int i = 0; i < totalClasses; i++){
            System.out.print("(" + labels[i] + ":" + prob_estimates[i] + ")");
        }
        System.out.println("(Actual:" + features[0] + " Prediction:" + v + ")"); 

        return v;
    }

    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        //     // variable so that it is not re-seeded every call.
        Random rand = new Random();
        //
        //             // nextInt is normally exclusive of the top value,
        //                 // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    private static double[][] generateDummyMap(){
        double[][] train = new double[1000][]; 
        //double[][] test = new double[10][];

        for (int i = 0; i < train.length; i++){
            if (i+1 > (train.length/2)){        // 50% positive
                double[] vals = {1,0,i+i};
                train[i] = vals;
            } else {
                double[] vals = {0,0,i-i-i-2}; // 50% negative
                train[i] = vals;
            }           
        }
        return train;
    }

    private static double[][] getTraining(Path training){
        return null;
    }

    public static void svmtask(InputStream in){
        double[][] train = generateDummyMap();        
        svm_model model = train(train);
        evaluate(train[randInt(0, 1000)], model);

        /*try {
            BufferedReader br= new BufferedReader(new InputStreamReader(in));
            String line;
            line=br.readLine();
            while (line != null){
                System.out.println(line);
                line=br.readLine();
            }
        } catch (final IOException ex){
        }*/
    }

    private static double[][] createMap(InputStream in){
        try {
            List<double[]> map = new ArrayList<double[]>();
            BufferedReader br= new BufferedReader(new InputStreamReader(in));
            String line;
            line=br.readLine();
            while (line != null){
                List<String> list = new LinkedList<String>(Arrays.asList(line.split(",")));
                Integer videoid = Integer.decode(list.remove(0));
                Integer shotid = Integer.decode(list.remove(0));
                double [] values = new double[list.size()];
                for (int i = 0; i < list.size(); i++){
                    values[i] = Double.valueOf(list.get(i));
                }
                map.add(values);
                line=br.readLine();
            }
            double [][] converted = new double[map.size()][];
            for (int i = 0; i < map.size(); i++){
                converted[i] = map.get(i);
            }
            return converted;

        } catch (final IOException ex){
        }
        return null;
    }

    private static double[][] concatJob(String directory, String labels) throws Exception {

        String in = directory + "/input";
        String out = directory + "/output";
        
        JobConf conf = new JobConf(SVM.class);
        conf.setJobName("svm");

        conf.setOutputKeyClass(FeatureKey.class);
        conf.setOutputValueClass(Text.class);

        conf.setMapperClass(Map.class);
        conf.setCombinerClass(Reduce.class);
        conf.setReducerClass(Reduce.class);

        conf.set("mapred.textoutputformat.separator", ",");
        conf.set("mapred.child.java.opts", "-Xmx1024m");
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        FileSystem fs = FileSystem.get(new Configuration());
        Path outpath = new Path(out);

        if (fs.exists(outpath)){
            fs.delete(outpath);
        }

        FileInputFormat.setInputPaths(conf, new Path(in));
        FileOutputFormat.setOutputPath(conf, outpath);

        JobClient.runJob(conf);

        FileStatus[] dirtree = fs.listStatus(outpath);

        for (int i=0; i < dirtree.length; i++){
            FileStatus status = dirtree[i];
            String filename = status.getPath().getName();
            if (filename.startsWith("_")){
                continue;
            }
            return createMap(fs.open(status.getPath()));
        }

        return null;
    }

    public static void main(String[] args) throws Exception {

        if (args.length < 3){
            System.out.println("Please provide three paramaters: training-dir testing-dir label-dir");
            System.exit(0);
        }

        String trainingDirectory = args[0];
        String testingDirectory = args[1];
        String labelsDirectory = args[2];
        
        // Run Training Job
        double[][] training = concatJob(trainingDirectory, labelsDirectory);

        svm_model model = train(training);
        evaluate(training[randInt(0, training.length)], model);

    }
}
