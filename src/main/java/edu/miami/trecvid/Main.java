package edu.miami.trecvid;

import edu.miami.trecvid.utilities.*;

/**
 * Main Class
 */
public class Main{
    
    /// Setup arguments
    private static void initArguments(ArgumentHandler arguments){
        arguments.add("edu.miami.trecvid.preprocessing.CheckCollection");
        arguments.add("edu.miami.trecvid.preprocessing.RenameVideos");
        arguments.add("edu.miami.trecvid.preprocessing.RenameAnn");
        arguments.add("edu.miami.trecvid.preprocessing.CreateLabels");
        arguments.add("edu.miami.trecvid.keyframes.GetIndex");
        arguments.add("edu.miami.trecvid.keyframes.ExtractFrames");
        arguments.add("edu.miami.trecvid.features.ExtractCedd");
        arguments.add("edu.miami.trecvid.features.ConcatFeatures");
        arguments.add("edu.miami.trecvid.classifiers.svm.Train");
        arguments.add("edu.miami.trecvid.classifiers.svm.Test");
    }
    
    public static void main(String [] args){
        ArgumentHandler arguments = ArgumentHandler.getInstance();
        
        initArguments(arguments);
        
        try {
            // Run instance
            arguments.execute(args);
        } catch (CallableException ex){
            System.out.println(String.format("Exception running command class %s. Reason: %s", ex.getClassName(), ex.getMessage()));
            System.exit(1);
        }
    }
}
