package edu.miami.trecvid.utilities;

import java.io.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Exception;

public class FeatureMap {

    public class FeatureMapException extends Exception {

    }

    SortedMap<Integer, SortedMap<Integer, List<Double> > > videomap = new TreeMap<Integer, SortedMap<Integer, List<Double> > >();

    public FeatureMap(){
    }

    public FeatureMap(String filename, boolean progress) throws FileNotFoundException, IOException{
        appendFile(filename, progress);
    }

    /**
     * Load from file
     * @param filename
     */
    public FeatureMap(String filename) throws FileNotFoundException, IOException{
        appendFile(filename);
    }

    public void add(Integer videoid, Integer shotid, List<Double> vector) {
        if (videomap.containsKey(videoid)) {
            SortedMap<Integer, List<Double> > shotmap = videomap.get(videoid);
            List<Double> list = null;
            if (shotmap.containsKey(shotid)){
                list = shotmap.get(shotid);
            } else {
                list = new ArrayList<Double>();
            }
            list.addAll(vector);
            shotmap.put(shotid, list);
            videomap.put(videoid, shotmap);
        } else {
            SortedMap shotmap = new TreeMap<Integer, List<Double> >();
            List<Double> list = new ArrayList<Double>();
            list.addAll(vector);
            shotmap.put(shotid, list);
            videomap.put(videoid, shotmap);
        }
    }

    public List<Double> lookup(Integer videoid, Integer shotid){
        if (videomap.containsKey(videoid)){
            SortedMap shotmap = videomap.get(videoid);
            if (shotmap != null && shotmap.containsKey(shotid)){
                return (List<Double>)shotmap.get(shotid);
            }
        }
        return null;
    }

    public void appendFile(String filename) throws FileNotFoundException, IOException{
        appendFile(filename, false);
    }

    public void appendFile(String filename, boolean progress) throws FileNotFoundException, IOException{
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            int count=0, total = Tools.countLines(filename);
            String line;
            while ((line = br.readLine()) != null) {
                insert(line);
                if (progress) {
                    Tools.printCompletionRate(count++, total, "Processing filename: " + filename);
                }
            }
            if (progress) {
                System.out.println();
            }
        }
    }

    private void insert(String line){
        List<String> list = new LinkedList<String>(Arrays.asList(line.split(",")));
        Integer videoid = Integer.decode(list.remove(0));
        Integer shotid = Integer.decode(list.remove(0));
        ArrayList<Double> values = new ArrayList<Double>();
        for (String item : list){
            values.add(Double.valueOf(item));
        }
        add(videoid, shotid, values);
    }

    public void dump(){
        for (Integer vkey : videomap.keySet()) {
            SortedMap<Integer, List<Double> > sortedmap = videomap.get(vkey);
            for (Integer skey : sortedmap.keySet()){
                System.out.printf("%d,%d", vkey, skey);
                for (Double value : sortedmap.get(skey)){
                    if (value.doubleValue() == value.longValue()) {
                        System.out.printf(",%d", value.intValue());
                    } else {
                        System.out.printf(",%s", value);
                    }
                }
                System.out.println();
            }
        }
    }

    public void writeToFile(String filename){
        writeToFile(filename, false);
    }

    public void writeToFile(String filename, boolean progress){
        try (PrintWriter output = new PrintWriter(new FileWriter(filename, false))){
            int mapsize = videomap.size(), count = 0;
            for (Integer vkey : videomap.keySet()) {
                SortedMap<Integer, List<Double> > shotmap = videomap.get(vkey);
                int shotsize = shotmap.size();
                for (Integer skey : shotmap.keySet()){
                    output.printf("%d,%d", vkey, skey);
                    for (Double value : shotmap.get(skey)){
                        if (value.doubleValue() == value.longValue()) {
                            output.printf(",%d", value.intValue());
                        } else {
                            output.printf(",%s", value);
                        }
                    }
                    output.println();
                }
                if (progress) {
                    Tools.printCompletionRate(count++, mapsize, "Writing videoid [" + vkey.toString() + "]");
                }
            }
            if (progress){
                System.out.println();
            }
            output.close();
        } catch (IOException ex){
        }
    }
}