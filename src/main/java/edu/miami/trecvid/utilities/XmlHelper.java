package edu.miami.trecvid.utilities;

import java.io.FilenameFilter;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XmlHelper{
    private String filename;
    private Document document;
    
    public XmlHelper(final String filename) throws XmlHelperException{
        try {
            this.filename = filename;
            DocumentBuilderFactory docbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = docbf.newDocumentBuilder();
            this.document = db.parse(filename);
            document.getDocumentElement().normalize();
        } catch (ParserConfigurationException ex){
            throw new XmlHelperException(String.format("Parser Configuration Exception! Reason: %s", ex.getMessage()));
        } catch (SAXException ex){
            throw new XmlHelperException(String.format("SAX Exception! Reason: %s", ex.getMessage()));
        } catch (IOException ex){
            throw new XmlHelperException(String.format("IO file Exception! Reason: %s", ex.getMessage()));
        }
    }
    
    public NodeList getElementsByTagName(final String tag){
        return document.getElementsByTagName(tag);
    }
}
