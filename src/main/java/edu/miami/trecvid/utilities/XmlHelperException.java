package edu.miami.trecvid.utilities;

import java.lang.Exception;

public class XmlHelperException extends Exception{
    public XmlHelperException(String message){
        super(message);
    }
}
