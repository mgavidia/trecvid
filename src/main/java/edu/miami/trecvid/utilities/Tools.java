package edu.miami.trecvid.utilities;

import java.io.Serializable;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.File;
import java.io.FilenameFilter;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.lang.StringBuffer;
import java.lang.ClassNotFoundException;
import java.lang.StringBuilder;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.nio.channels.FileChannel;

public class Tools{
     /* Join string by delimiter */
    public static String join(List<String> list, String delim) {
        StringBuilder sb = new StringBuilder();
        String loopDelim = "";
        for(String s : list) {
            sb.append(loopDelim);
            sb.append(s);            
            loopDelim = delim;
        }
        return sb.toString();
    }
    
    /* Save a List to File */
    public static <T> void saveListToFile(String filename, List<T> list, Boolean... params) throws IOException{
        assert params.length <= 1;
        boolean erase = params.length > 0 ? params[0].booleanValue() : false;
        File f = new File(filename);
        if (erase && f.exists()){
            f.delete();
        }
        OutputStream file = new FileOutputStream(f);
        OutputStream buffer = new BufferedOutputStream(file);
        ObjectOutput output = new ObjectOutputStream(buffer);
        try{
            output.writeObject(list);
        } finally {
            output.close();
        }
    }
    
    /* Load a list from file */
    public static <T> List<T> loadListFromFile(String filename) throws IOException{
        try{
            InputStream file = new FileInputStream(filename);
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);
            try {
                return (List<T>)input.readObject();
            } finally {
                input.close();
            }
        } catch (ClassNotFoundException ex){
        }
        return null;
    }
    
    /* Save a map to file */
    public static <K, V> void saveMapToFile(String filename, Map<K, V> map, Boolean... params) throws IOException{
        assert params.length <= 1;
        boolean erase = params.length > 0 ? params[0].booleanValue() : false;
        File f = new File(filename);
        if (erase && f.exists()){
            f.delete();
        }
        OutputStream file = new FileOutputStream(f);
        OutputStream buffer = new BufferedOutputStream(file);
        ObjectOutput output = new ObjectOutputStream(buffer);
        try{
            output.writeObject(map);
        } finally {
            output.close();
        }
    }
    
    /* Load a map from file */
    public static <K, V> Map<K, V> loadMapFromFile(String filename) throws IOException{
        try{
            InputStream file = new FileInputStream(filename);
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);
            try {
                return (Map<K, V>)input.readObject();
            } finally {
                input.close();
            }
        } catch (ClassNotFoundException ex){
        }
        
        return null;
    }
    
    /* Grab a List of files from a given path */
    public static List<File> getFileList(String path, String extension){
        class Filter implements FilenameFilter{
            Filter(String extension){
                this.extension = extension;
            }
            public boolean accept(File dir, String name){
                return name.toLowerCase().endsWith(extension);
            }
            String extension;
        }
        List<File> files = new ArrayList<File>(Arrays.asList((new File(path)).listFiles(new Filter(extension))));
        
        return files;
    }
    
    /* Get a key by a given value in a map */
    public static <K, V> K getKeyByValue(Map<K, V> map, V value){
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
    
    /* copy a file */
    public static void copyFile(File sourceFile, File destFile) throws IOException{
        if(!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        finally {
            if(source != null){
                source.close();
            }
            if(destination != null){
                destination.close();
            }
        }
    }
    
    public static String fileToString(File file) throws IOException{
        int len;
        char[] chr = new char[4096];
        final StringBuffer buffer = new StringBuffer();
        final FileReader reader = new FileReader(file);
        try{
          while ((len = reader.read(chr)) > 0) {
              buffer.append(chr, 0, len);
          }
        } finally {
          reader.close();
        }
        
        return buffer.toString();
    }
    
    public static void printCompletionRate(int current, int max, String... params){
        assert params.length <= 1;
        String filename = params.length > 0 ? params[0] : "";
        int percent = 101*current/max;
        if (filename.isEmpty()){
            System.out.print(String.format("------------- %d %% completed ---------------\r" + (percent >= 101 ? "\n" : ""),percent));
        } else {
            System.out.print(String.format("------------- %s - %d %% completed ---------------\r" + (percent >= 101 ? "\n" : ""),filename, percent));
        }
    }
    
    public static int countLines(String filename) throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(filename));
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } finally {
            is.close();
        }
    }
    
    public static <T> boolean writeListToFile(List<T> list, String filename){
        try{
            PrintWriter output = new PrintWriter(new FileWriter(filename));
            for (T item : list){
                output.println(item);
            }
            output.close();
        } catch (IOException ex){
            return false;
        }
        return true;
    }

    public static void deleteDirectory(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteDirectory(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }
}
