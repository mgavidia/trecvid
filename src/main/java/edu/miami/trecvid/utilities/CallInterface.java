package edu.miami.trecvid.utilities;

import java.util.List;

public interface CallInterface{
    
    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException;
    
    // Get Description
    public String description();
    
    // Get keywords that will invoke this instance
    public List<String> keywords();
    
    // Get Argument list
    public List<String> arguments();
}
