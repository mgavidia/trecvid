package edu.miami.trecvid.utilities;

import java.lang.Exception;

public class CallableException extends Exception{
    private String className;
    public CallableException(String message, String className){
        super(message);
        this.className = className;
    }

    public String getClassName(){
        return className;
    }
}
