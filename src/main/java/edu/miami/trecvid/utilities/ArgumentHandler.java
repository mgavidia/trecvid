package edu.miami.trecvid.utilities;

import edu.miami.trecvid.Main;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.lang.StringBuilder;
import java.lang.ClassNotFoundException;
import java.lang.InstantiationException;
import java.lang.IllegalAccessException;

public class ArgumentHandler{
    
    static ArgumentHandler instance = new ArgumentHandler();
    
    /// index counter for lookups
    private int CLASS_INDEX = 0;
    
    /// Keywords and lookup
    private Map<String, Integer> classMap = new HashMap<String, Integer>();
    
    /// Place all keywords in a string
    private Map<Integer, String> keywordsAsString = new HashMap<Integer, String>();
    
    /// Place all arguments in a string
    private Map<Integer, String> argumentsAsString = new HashMap<Integer, String>();
    
    /// classname lookup
    private Map<Integer, String> classLookup = new HashMap<Integer, String>();
    
    /// Description lookup
    private Map<Integer, String> descriptionLookup = new HashMap<Integer, String>();
    
    public void add(String className){
        try {
            Class<?> check = Class.forName(className, false , this.getClass().getClassLoader());
            CallInterface callable = (CallInterface)check.newInstance();
            // Check to make sure this is a valid class
            if (!(callable instanceof CallInterface)){
                throw new Exception(String.format("Problem adding class '%s' since it does not implement CallInterface", className));
            }
            
            // Create keywords string
            StringBuilder builder = new StringBuilder();
            
            for (String keyword : callable.keywords()){
                classMap.put(keyword, CLASS_INDEX);
                builder.append(keyword + ", ");
            }
            builder.delete(builder.length()-2, builder.length());
            
            keywordsAsString.put(CLASS_INDEX, builder.toString());
            
            // Create argument list
            builder = new StringBuilder();
            
            for (String arguments : callable.arguments()){
                builder.append(" [" + arguments + "]");
            }
            
            argumentsAsString.put(CLASS_INDEX, builder.toString());
            
            classLookup.put(CLASS_INDEX, className);
            
            descriptionLookup.put(CLASS_INDEX, callable.description());
            
            // Increment for next item
            CLASS_INDEX++;
        } catch (ClassNotFoundException ex){
            System.out.println(String.format("[INFO] Class '%s' not found. Ignoring.", className));
        } catch (InstantiationException ex){
            System.out.println(String.format("Couldn't create an instance of class '%s'. Reason: %s", className, ex.getMessage()));
            System.exit(1);
        } catch (IllegalAccessException ex){
            System.out.println(String.format("Illegal access exception in class '%s'. Reason: %s", ex.getMessage()));
            System.exit(1);
        } catch (Exception ex){
            System.out.println(String.format("Exception caught. Reason: %s", ex.getMessage()));
            System.exit(1);
        }
    }
    
    public String formatArgument(int index){
        return String.format("keywords <%s> : arguments%s : description: %s", keywordsAsString.get(index), argumentsAsString.get(index), descriptionLookup.get(index));
    }
    
    // Print arguments
    public void printArguments(){
        System.out.println("usage: " + Main.class.getName() + " <keyword> {arguments}");
        for (int i =0; i < CLASS_INDEX; i++){
            System.out.println(formatArgument(i));
        }
        System.exit(0);
    }
    
    public void execute(String [] args) throws CallableException{
        if (args.length == 0){
            printArguments();
        }
        
        Integer index = classMap.get(args[0]);
        
        if (index == null){
            printArguments();
        }
        
        String [] newArgs = new String[args.length - 1];
        for (int i = 1; i < args.length; i++){
            newArgs[i-1] = args[i];
        }
        String className = classLookup.get(index);
        
        try {
            // Run instance
            CallInterface callable = (CallInterface)Class.forName(className).newInstance();
            callable.run(newArgs);
        } catch (ArgumentException ex){
            System.out.println("Error running command. Reason: " + ex.getMessage());
            System.out.println("Usage for command:");
            System.out.println(formatArgument(index));
            System.exit(1);
        }  catch (ClassNotFoundException ex){
            throw new CallableException(className, ex.getMessage());
        } catch (InstantiationException ex){
            throw new CallableException(className, ex.getMessage());
        } catch (IllegalAccessException ex){
            throw new CallableException(className, ex.getMessage());
        }
    }
    
    static public ArgumentHandler getInstance(){
        return instance;
    }
    
    private ArgumentHandler(){
    }
}
