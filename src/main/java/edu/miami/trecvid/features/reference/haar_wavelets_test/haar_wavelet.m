function feature6 = haar_wavelet( image ) 
% clc;clear all;
% image=imread('test.jpg');
level = 4;
type = 'haar';

tempf = zeros(10,26); %(4 levels *3 + 1 ),(mean + var)*(9 blocks + 1 global)=10,26
double_image = histeq(im2double(rgb2gray(image)));

ngr = 3;
block_size = fix(size(double_image)/ngr); %Size of each block for features

for i=1:ngr
    for j=1:ngr
        
        r = block_size(1)*(i-1)+1;
        c = block_size(2)*(j-1)+1;

        block = double_image(r:r+block_size(1)-1,c:c+block_size(2)-1,:);
        %imshow(block)
        % coefficient = [ cA4 | cH4 | cV4 | cD4 | cH3 | cV3 | cD3 | cH2 | cV2 | cD2 | cH1 | cV1 | cD1]
        [coefficient,record] = wavedec2(block,level,type);  

        start_step = record(1,1)*record(1,2);
        mean_cA4 = mean(coefficient(1:start_step));
        var_cA4 = var(coefficient(1:start_step));
        
        mean_cH = zeros(1,4);
        mean_cV = zeros(1,4);
        mean_cD = zeros(1,4);
        var_cH = zeros(1,4);
        var_cV = zeros(1,4);
        var_cD = zeros(1,4);
        for k = 1:level
            end_step = start_step + record((k+1),1)*record((k+1),2);
            mean_cH(1,k) = mean(coefficient(start_step+1:end_step));
            var_cH(1,k) = var(coefficient(start_step+1:end_step));
            
            start_step = end_step;
            end_step = start_step + record((k+1),1)*record((k+1),2);
            mean_cV(1,k) = mean(coefficient(start_step+1:end_step));
            var_cV(1,k) = var(coefficient(start_step+1:end_step));
            
            start_step = end_step;
            end_step = start_step + record((k+1),1)*record((k+1),2);
            mean_cD(1,k) = mean(coefficient(start_step+1:end_step));
            var_cD(1,k) = var(coefficient(start_step+1:end_step));
        end
        tempf((i-1)*ngr+j,:) = [mean_cA4 mean_cH mean_cV mean_cD var_cA4 var_cH var_cV var_cD]; 
    end
end

% for global image
[coefficient,record] = wavedec2(double_image,level,type);  

start_step = record(1,1)*record(1,2);
mean_cA4 = mean(coefficient(1:start_step));
var_cA4 = var(coefficient(1:start_step));

mean_cH = zeros(1,4);
mean_cV = zeros(1,4);
mean_cD = zeros(1,4);
var_cH = zeros(1,4);
var_cV = zeros(1,4);
var_cD = zeros(1,4);
for k = 1:level
    end_step = start_step + record((k+1),1)*record((k+1),2);
    mean_cH(1,k) = mean(coefficient(start_step+1:end_step));
    var_cH(1,k) = var(coefficient(start_step+1:end_step));

    start_step = end_step;
    end_step = start_step + record((k+1),1)*record((k+1),2);
    mean_cV(1,k) = mean(coefficient(start_step+1:end_step));
    var_cV(1,k) = var(coefficient(start_step+1:end_step));

    start_step = end_step;
    end_step = start_step + record((k+1),1)*record((k+1),2);
    mean_cD(1,k) = mean(coefficient(start_step+1:end_step));
    var_cD(1,k) = var(coefficient(start_step+1:end_step));
end

tempf(10,:) = [mean_cA4 mean_cH mean_cV mean_cD var_cA4 var_cH var_cV var_cD];    
temp = tempf';
feature6=temp(:)';

clear level;
clear type;
clear double_image;
clear ngr
clear block;
clear r;
clear c;
clear block_size;
clear i;
clear j;
clear k;
clear mean_cA4;
clear var_cA4;
clear mean_cH
clear var_cH;
clear mean_cV;
clear var_cV;
clear start_step;
clear end_step;
clear mean_cD;
clear var_cD;
clear temp;
clear tempf;
clear record;
clear coefficient;


        
            
  



