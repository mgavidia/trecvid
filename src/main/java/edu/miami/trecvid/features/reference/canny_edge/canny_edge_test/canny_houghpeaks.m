function feature4 = canny_houghpeaks( image )

gray_image = rgb2gray(image);
double_image = histeq(im2double(gray_image));

% get binary image from 'canny' 
edge_image1=edge(double_image,'canny');

% Hough transform
[H,ThetaResolution,RhoResolution] = hough(edge_image1);
% Get top200 peaks in hough transform matrix
Peaks  = houghpeaks(H,200,'threshold',ceil(0.2*max(H(:))));
% Get 18 bins histogram of theta and Rho
% The number of feature is 36
% sometime less than 200 peaks are extracted, so calculate the Actual Number of Peaks
ANP = size(Peaks,1);
feature4 = [hist(ThetaResolution(Peaks(:,2)),18) hist(RhoResolution(Peaks(:,1)),18)]/ANP;

clear ANP;
clear gray_image;
clear double_image;
clear edge_image1;
clear H;
clear ThetaResolution;
clear RhoResolution;
clear Peaks;