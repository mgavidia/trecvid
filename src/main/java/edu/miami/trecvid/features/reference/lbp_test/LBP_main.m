function feature = LBP_main(image)

%mapping=getmapping(8,'u2'); %       'u2'   for uniform LBP
mapping=getmapping(8,'ri'); %       'ri'   for rotation-invariant LBP
feature=lbp(image,1,8,mapping,'nh');

end