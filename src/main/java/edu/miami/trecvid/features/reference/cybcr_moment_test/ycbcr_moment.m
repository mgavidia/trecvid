function feature3 = ycbcr_moment(image)

% Global + 3*3 blocks = 10 components
feature3=zeros(1,120);
ngr = 3;
ycbcr_image = double(rgb2ycbcr(image));
block_size = fix(size(ycbcr_image(:,:,1))/ngr); %Size of each block for features

kk = 1;
% Compute blocks
for i=1:ngr
    for j=1:ngr
        %Pointer to locate image blocks' pixels
        r = block_size(1)*(i-1)+1;
        c = block_size(2)*(j-1)+1;
        block = ycbcr_image(r:r+block_size(1)-1,c:c+block_size(2)-1,:);
        
        temp    = block(:,:,1);      block1  = temp(:);        
        temp    = block(:,:,2);      block2  = temp(:);
        temp    = block(:,:,3);      block3  = temp(:);
      
        k = 12*(kk-1); %Pointer to locate Color Moments features in vector
        
        feature3(k+1) = mean( block1 );
        feature3(k+2) = mean( block2 );
        feature3(k+3) = mean( block3 );
                
        feature3(k+4) = var( block1 );
        feature3(k+5) = var( block2 );
        feature3(k+6) = var( block3 );
         
        if feature3(k+4)==0
            feature3(k+7) = 0;
            feature3(k+10) = -1.2;
        else
            feature3(k+7) = skewness( block1 );
            feature3(k+10) = kurtosis( block1 );
        end        
        
        if feature3(k+5)==0
            feature3(k+8) = 0;
            feature3(k+11) = -1.2;
        else
            feature3(k+8) = skewness( block2 );
            feature3(k+11) = kurtosis( block2 );
        end
        
        if feature3(k+6)==0
            feature3(k+9) = 0;
            feature3(k+12) = -1.2;
        else
            feature3(k+9) = skewness( block3 );
            feature3(k+12) = kurtosis( block3 );
        end
        
        kk = kk+1;

    end
end

% Compute global mean, var, skewness, kurtosis
k = 108;
feature3(k+1) = mean( block1 );
feature3(k+2) = mean( block2 );
feature3(k+3) = mean( block3 );

feature3(k+4) = var( block1 );
feature3(k+5) = var( block2 );
feature3(k+6) = var( block3 );

if feature3(k+4)==0
    feature3(k+7) = 0;
    feature3(k+10) = -1.2;
else
    feature3(k+7) = skewness( block1 );
    feature3(k+10) = kurtosis( block1 );
end        

if feature3(k+5)==0
    feature3(k+8) = 0;
    feature3(k+11) = -1.2;
else
    feature3(k+8) = skewness( block2 );
    feature3(k+11) = kurtosis( block2 );
end

if feature3(k+6)==0
    feature3(k+9) = 0;
    feature3(k+12) = -1.2;
else
    feature3(k+9) = skewness( block3 );
    feature3(k+12) = kurtosis( block3 );
end

clear k;
clear ngr;
clear ycbcr_image;
clear block_size;
clear r;
clear c;
clear kk;
clear i;
clear j;
clear block;
clear block1;
clear block2;
clear block3;
