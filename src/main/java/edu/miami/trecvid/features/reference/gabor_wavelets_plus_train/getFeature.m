function [ output_feature, problem ] = getFeature(input_dir, videoID, num_Of_Features)
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

% list out all the ".jpg" file in the directory
file_List = dir(fullfile(input_dir,'*.jpg'));

problem = [];

% get the size of the file_List
[r c] = size(file_List);

% initialize the feature Matrix
featureMatrix = zeros(r,num_Of_Features+2);

% iterate the file_List
for i = 1: r
    
    % get the fileName shot1703_18_RKF.jpg
    fileName = ['shot' num2str(videoID) '_' num2str(i) '_RKF.jpg'];
    
    %file_List(i).name;
    
    % get the fullfileName
    fullFileName = fullfile(input_dir,fileName);
    
    % check whether this file exists
    if exist(fullFileName)==0
        
        currentProblem = [videoID i];
        problem = [problem;currentProblem]
        continue; 
       
    end
    
    % read the file in
    inputImage = imread(fullFileName,'jpg');
    
    % parse the videoID and shotID
    %[videoID remain] = strtok(fileName,'_'); 
    
   % videoID = regexprep(videoID,'[a-z]*|[A-Z]*', '');
    
    % convert video_ID to number
    %videoID = str2num(videoID);
    
    %[shotID remain2] = strtok(remain, '_');
    
    % convert shotID to number
    %shotID = str2num(shotID);
    
    
    % current feature vector
    currentFeatureVector = zeros(1,num_Of_Features+2);
    
    currentFeatureVector(1,1) = videoID;
    currentFeatureVector(1,2) = i;
    
    currentFeatureVector(1,3:end) = texture_gabor_plus(inputImage);
    
    
    % put currentFeatureVector into featureMatrix
    featureMatrix(i,:) = currentFeatureVector;
    
      
end

output_feature = featureMatrix;

end

