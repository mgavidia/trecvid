function feature = edge_sobel_histogram( image )

gray_image = rgb2gray(image);
double_image = histeq(im2double(gray_image));

%edge of image by sobel filter
[edge_image2,thre]=edge(double_image,'sobel');

ngr = 3;
block_size = fix(size(edge_image2)/ngr); %Size of each block for features
area = block_size(1)*block_size(2);

feature = zeros(1,50);
eHist2 = zeros(1,20);
eHist3 = zeros(1,20);
eHist4 = zeros(1,20);
eHist5 = zeros(1,20);
eHist6 = zeros(1,20);
for i=1:ngr
    for j=1:ngr 
        %Pointer to locate image blocks' pixels
        r = block_size(1)*(i-1)+1;
        c = block_size(2)*(j-1)+1;
        block = edge_image2(r:r+block_size(1)-1,c:c+block_size(2)-1,:);

        gv = imfilter(block,fspecial('sobel')'/8,'circular');
        eHist2((i-1)*ngr*2+(j-1)*2+1:(i-1)*ngr*2+(j-1)*2+2) = imhist(gv,2)/area;

        gh = imfilter(block,fspecial('sobel') /8,'circular'); 
        eHist3((i-1)*ngr*2+(j-1)*2+1:(i-1)*ngr*2+(j-1)*2+2) = imhist(gh,2)/area;

        g45  = imfilter(block,[1 0; 0 -1]/2,'circular');
        eHist4((i-1)*ngr*2+(j-1)*2+1:(i-1)*ngr*2+(j-1)*2+2) = imhist(g45,2)/area;

        g135 = imfilter(block,[0 1;-1  0]/2,'circular');
        eHist5((i-1)*ngr*2+(j-1)*2+1:(i-1)*ngr*2+(j-1)*2+2) = imhist(g135,2)/area;
        
        eHist6((i-1)*ngr*2+(j-1)*2+1:(i-1)*ngr*2+(j-1)*2+2) = (area - (imhist(gv,2)+imhist(gh,2)+imhist(g45,2)+imhist(g135,2)))/area;
    end
end


%Globle features
areah = size(edge_image2,1)*size(edge_image2,2);

gv = imfilter(edge_image2,fspecial('sobel')'/8,'circular');
eHist2(ngr*ngr*2+1:ngr*ngr*2+2) = imhist(gv,2)/areah;

gh = imfilter(edge_image2,fspecial('sobel') /8,'circular'); 
eHist3(ngr*ngr*2+1:ngr*ngr*2+2) = imhist(gh,2)/areah;

g45  = imfilter(edge_image2,[1 0; 0 -1]/2,'circular');
eHist4(ngr*ngr*2+1:ngr*ngr*2+2) = imhist(g45,2)/areah;

g135 = imfilter(edge_image2,[0 1;-1  0]/2,'circular');
eHist5(ngr*ngr*2+1:ngr*ngr*2+2) = imhist(g135,2)/areah;

eHist6(ngr*ngr*2+1:ngr*ngr*2+2) = (areah - (imhist(gv,2)+imhist(gh,2)+imhist(g45,2)+imhist(g135,2)))/areah;

% save the edge information, ignor the background
feature(0*ngr*ngr+1:1*(ngr*ngr+1)) = eHist2(2:2:(2*ngr*ngr+2));
feature(1*(ngr*ngr+1)+1:2*(ngr*ngr+1)) = eHist3(2:2:(2*ngr*ngr+2));
feature(2*(ngr*ngr+1)+1:3*(ngr*ngr+1)) = eHist4(2:2:(2*ngr*ngr+2));
feature(3*(ngr*ngr+1)+1:4*(ngr*ngr+1)) = eHist5(2:2:(2*ngr*ngr+2));
feature(4*(ngr*ngr+1)+1:5*(ngr*ngr+1)) = eHist6(2:2:(2*ngr*ngr+2));

clear gray_image;
clear double_image;
clear edge_image2;
clear eHist2;
clear eHist3;
clear eHist4;
clear eHist5;
clear eHist6;
clear thre;
clear gv;
clear gh;
clear g45;
clear g135;
clear ngr;
clear block_size;
clear r;
clear c;
clear block;
clear area;
clear areah;