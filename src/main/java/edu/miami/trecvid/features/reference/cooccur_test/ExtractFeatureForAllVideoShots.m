%% Generate Feature for all the images

%% Input:
%% modify the following parameter:
%% inputDir: the directory holding all the folds of each video
%% outputFeature: the path for the generated feature file in .csv format, the first column is videoID, the second column is shotID
%% outputErrorShotFrame: the path for the video ID and shot ID corresponding to the shots which do not exist
%% outputDirNotExist: the path the videoID of those videos which are missing
%% num_Of_Features: total dimension of features
%% numOfDir: the maximum ID of the videos, be careful that it might be greater than 
%%           the total number of videos, for example, in TRECVID2011 training data,
%%           total number of videos is 11641, however, the maximum ID of the videos
%%           is 11644.

%% output:
%% outputFeature: the generated feature file in .csv format, the first column is videoID, the second column is shotID
%% outputErrorShotFrame: the video IDs and shot IDs corresponding to the shots which do not exist
%% outputDirNotExist: the videoIDs of those videos which are missing


% house keeping

clear all;
clc;

%% Input dir
% tv11_train
%inputDir = '/nethome/tmeng/HOGFeature/ExtractHogForTV10OriginalSize/Data/keyframeOriginalRawImage/Keyframes_11train';
% tv11_test
%inputDir = '/nethome/tmeng/Keyframes_11test';
% tv12_test
inputDir = 'media/data-share/TRECVID/TRECVID_2014/iacc.2.b.iframes/tv13' %'/nethome/tmeng/TRECVIDData/TRECVID2012_Testing_OriginalSize_keyframes/tv12_keyframes_orig_size';

outputFeature = '/media/data-share/TRECVID/TRECVID_2014/iacc.2.b.features/cooccur/cooccur_test.csv' 
%'./out.csv'%'/nethome/dliu4/tv12_feature_extraction/tv12_test_features/cooccur/TV12Te_cooccur_Fea36_OS.csv';

outputErrorShotFrame = './outerror.csv' %'/nethome/dliu4/tv12_feature_extraction/tv12_test_features/cooccur/TRECVID2012_cooccur_Error_Shot_Original_Size.csv';

outputDirNotExist = './outnotexist.csv'%'/nethome/dliu4/tv12_feature_extraction/tv12_test_features/cooccur/TRECVID2012_cooccur_Feature_Dir_Not_Exist_Original_Size.csv';

%% list how many dir are there
%% need to modify the "numOfDir"
%% numOfDir is the largest number of directory number
%numOfDir = 11644;

%% num of features
num_Of_Features = 36;

%% initialize the feature matrix
featureMatrix = [];
errorShotFrame = [];
dirNotExist = [];


for i = 30544:32939
    
    inputDirForI = [num2str(i)];
    currentDirectoryName = fullfile(inputDir,inputDirForI);
    
    %check exist
    if exist(currentDirectoryName)==0
        
        dirNotExist = [dirNotExist;i];
        
        
    end    
     
    disp(['next' num2str(i)])
    
    %extract feature
    [currentFeature currentError] = getFeature(currentDirectoryName, i, num_Of_Features);
    
    featureMatrix = [featureMatrix;currentFeature];
    
    if ~isempty(currentError)
       
       errorShotFrame= [errorShotFrame;currentError];
    
    end
    
end

csvwrite(outputFeature,featureMatrix);
csvwrite(outputErrorShotFrame,errorShotFrame);
csvwrite(outputDirNotExist,dirNotExist);
