
function feature8 = texture_gabor_plus( image )
%calculte the gabor-transformation-based features of given image
Num_Scale = 3;
Num_Orientation = 4;
MinWaveLength = 3;
Mult = 2; 
SigmaOnf = 0.65;
DThetaOnSigma = 1.5;

ImageDouble=im2double(image);
ImageSize=size(ImageDouble);
SquareEdgeLength=min(ImageSize(1:2));
exponent=1;
while 1
    if 2^exponent>SquareEdgeLength
        break;
    else
        exponent=exponent+1;
    end
end
ActualEdgeLength=2^(exponent-1);
VerticalOffset = floor((ImageSize(1)-ActualEdgeLength)/2);
HorizontalOffset = floor((ImageSize(2)-ActualEdgeLength)/2);
feature8=zeros(1,9*Num_Orientation*Num_Scale);

% 1-> center subimage
SquarePart = ImageDouble(VerticalOffset+1:VerticalOffset+ActualEdgeLength,HorizontalOffset+1:HorizontalOffset+ActualEdgeLength,:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM1 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM1( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(0*Num_Scale*Num_Orientation+1:1*Num_Scale*Num_Orientation) = SUM1(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;

% 2-> left-center image
SquarePart=ImageDouble(VerticalOffset+1:VerticalOffset+ActualEdgeLength, 1:ActualEdgeLength,:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM2 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM2( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(1*Num_Scale*Num_Orientation+1:2*Num_Scale*Num_Orientation) = SUM2(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;

% 3-> left-up subimage
SquarePart=ImageDouble(1:ActualEdgeLength,1:ActualEdgeLength,:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM3 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM3( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(2*Num_Scale*Num_Orientation+1:3*Num_Scale*Num_Orientation) = SUM3(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;


% 4-> buttom-center image
SquarePart=ImageDouble(ImageSize(1)-ActualEdgeLength+1:ImageSize(1),HorizontalOffset+1:HorizontalOffset+ActualEdgeLength,:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM4 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM4( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(3*Num_Scale*Num_Orientation+1:4*Num_Scale*Num_Orientation) = SUM4(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;


% 5-> left-bottom subimage
SquarePart=ImageDouble(ImageSize(1)-ActualEdgeLength+1:ImageSize(1),1:ActualEdgeLength,:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM5 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM5( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(4*Num_Scale*Num_Orientation+1:5*Num_Scale*Num_Orientation) = SUM5(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;


% 6-> right_center subimage
SquarePart=ImageDouble(VerticalOffset+1:VerticalOffset+ActualEdgeLength, ImageSize(2)-ActualEdgeLength+1:ImageSize(2),:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM6 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM6( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(5*Num_Scale*Num_Orientation+1:6*Num_Scale*Num_Orientation) = SUM6(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;

% 7-> right-up subimage
SquarePart=ImageDouble(1:ActualEdgeLength,ImageSize(2)-ActualEdgeLength+1:ImageSize(2),:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM7 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM7( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(6*Num_Scale*Num_Orientation+1:7*Num_Scale*Num_Orientation) = SUM7(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;

% 8-> up-center subimage
SquarePart=ImageDouble(1:ActualEdgeLength,HorizontalOffset+1:HorizontalOffset+ActualEdgeLength,:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM8 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM8( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(7*Num_Scale*Num_Orientation+1:8*Num_Scale*Num_Orientation) = SUM8(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;

% 9-> right-bottom subimage
SquarePart=ImageDouble(ImageSize(1)-ActualEdgeLength+1:ImageSize(1),ImageSize(2)-ActualEdgeLength+1:ImageSize(2),:);
GrayImage=histeq(rgb2gray(SquarePart));
Gabor_Data=gaborconvolve(GrayImage,Num_Scale,Num_Orientation,MinWaveLength,Mult,SigmaOnf,DThetaOnSigma);
SUM9 = zeros(1,Num_Scale*Num_Orientation);
for i=1:1:Num_Scale
    for j=1:1:Num_Orientation
        temp_vector = cell2mat(Gabor_Data(i,j));
        SUM9( (i-1)*Num_Orientation+j ) = sum(temp_vector(:));
    end
end
feature8(8*Num_Scale*Num_Orientation+1:9*Num_Scale*Num_Orientation) = SUM9(1:Num_Scale*Num_Orientation)/(ActualEdgeLength^2)*1000;

clear Num_Scale;
clear Num_Orientation;
clear MinWaveLength;
clear Mult; 
clear SigmaOnf;
clear DThetaOnSigma;
clear ImageDouble;
clear ImageSize;
clear SquareEdgeLength;
clear exponent;
clear ActualEdgeLength;
clear VerticalOffset;
clear HorizontalOffset;
clear SquarePart;
clear GrayImage;
clear Gabor_Data;
clear SUM1;
clear SUM2;
clear SUM3;
clear SUM4;
clear SUM5;
clear SUM6;
clear SUM7;
clear SUM8;
clear SUM9;
%------------------------------------------------------------------------------------

function EO = gaborconvolve(im, nscale, norient, minWaveLength, mult,sigmaOnf, dThetaOnSigma)
    
if ~isa(im,'double')
    im = double(im);
end
    
[rows cols] = size(im);					
imagefft = fft2(im);                 % Fourier transform of image
EO = cell(nscale, norient);          % Pre-allocate cell array

% Pre-compute some stuff to speed up filter construction

[x,y] = meshgrid( [-cols/2:(cols/2-1)]/cols,[-rows/2:(rows/2-1)]/rows);
radius = sqrt(x.^2 + y.^2);       % Matrix values contain *normalised* radius from centre.
radius(round(rows/2+1),round(cols/2+1)) = 1; % Get rid of the 0 radius value in the middle 
                                             % so that taking the log of the radius will 
                                             % not cause trouble.

% Precompute sine and cosine of the polar angle of all pixels about the
% centre point					     

theta = atan2(-y,x);              % Matrix values contain polar angle.
                                  % (note -ve y is used to give +ve
                                  % anti-clockwise angles)
sintheta = sin(theta);
costheta = cos(theta);
clear x; clear y; clear theta;      % save a little memory

thetaSigma = pi/norient/dThetaOnSigma;  % Calculate the standard deviation of the
                                        % angular Gaussian function used to
                                        % construct filters in the freq. plane.
% The main loop...

for o = 1:norient,                   % For each orientation.

  angl = (o-1)*pi/norient;           % Calculate filter angle.
  wavelength = minWaveLength;        % Initialize filter wavelength.

  % Pre-compute filter data specific to this orientation
  % For each point in the filter matrix calculate the angular distance from the
  % specified filter orientation.  To overcome the angular wrap-around problem
  % sine difference and cosine difference values are first computed and then
  % the atan2 function is used to determine angular distance.

  ds = sintheta * cos(angl) - costheta * sin(angl);     % Difference in sine.
  dc = costheta * cos(angl) + sintheta * sin(angl);     % Difference in cosine.
  dtheta = abs(atan2(ds,dc));                           % Absolute angular distance.
  spread = exp((-dtheta.^2) / (2 * thetaSigma^2));      % Calculate the angular filter component.

  for s = 1:nscale,                  % For each scale.

    % Construct the filter - first calculate the radial filter component.
    fo = 1.0/wavelength;                  % Centre frequency of filter.

    logGabor = exp((-(log(radius/fo)).^2) / (2 * log(sigmaOnf)^2));  
    logGabor(round(rows/2+1),round(cols/2+1)) = 0; % Set the value at the center of the filter
                                                   % back to zero (undo the radius fudge).

    filter = fftshift(logGabor .* spread); % Multiply by the angular spread to get the filter
                                           % and swap quadrants to move zero frequency 
                                           % to the corners.

    % Do the convolution, back transform, and save the result in EO
    EO{s,o} = abs(ifft2(imagefft .* filter));    

    wavelength = wavelength * mult;       % Finally calculate Wavelength of next filter
  end                                     % ... and process the next scale

end  % For each orientation

clear im;
clear rows;
clear cols;
clear imagefft;
clear sintheta;
clear costheta;
clear radius;
clear thetaSigma;
clear norient;
clear angl;
clear wavelength;
clear ds;
clear dc;
clear dtheta;
clear spread;
clear nscale;
clear fo;
clear logGabor;
clear filter;








