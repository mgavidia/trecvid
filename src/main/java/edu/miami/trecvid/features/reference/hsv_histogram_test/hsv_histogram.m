function feature2 = hsv_histogram(image)

num_pixel = size(image,1)*size(image,2);
nBins = 16;
hsv_image = rgb2hsv(image);

rHist = imhist(hsv_image(:,:,1), nBins);
gHist = imhist(hsv_image(:,:,2), nBins);
bHist = imhist(hsv_image(:,:,3), nBins);

feature2 = [rHist' gHist' bHist']/num_pixel;

clear nBins;
clear hsv_image;
clear rHist;
clear gHist;
clear bHist;
