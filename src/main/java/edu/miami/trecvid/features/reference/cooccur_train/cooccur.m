function feature5 = cooccur( image )
%calculate the co-occurrence matrix
feature5 = zeros(1,36);
Offset=[0 1;-1 1;-1 0;-1 -1];

gray_image = histeq(im2uint8(rgb2gray(image)));
cooccur_matrix = graycomatrix(gray_image,'NumLevel',8,'Offset',Offset);

size_glcm_1 = size(cooccur_matrix,1);
size_glcm_2 = size(cooccur_matrix,2);
size_glcm_3 = size(cooccur_matrix,3);

glcm = zeros(size_glcm_1,size_glcm_2,size_glcm_3);
glcm_sum = zeros(size_glcm_3,1);
glcm_max = zeros(size_glcm_3,1);

energy = zeros(1,size_glcm_3); 
contrast = zeros(1,size_glcm_3); 
entropy =  zeros(1,size_glcm_3); 
homogeneity = zeros(1,size_glcm_3);

correlation = zeros(1,size_glcm_3);
auto_correlation = zeros(1,size_glcm_3);
cluster_prominence = zeros(1,size_glcm_3);
cluster_shade = zeros(1,size_glcm_3);

u_x = zeros(size_glcm_3,1);
u_y = zeros(size_glcm_3,1);
s_x = zeros(size_glcm_3,1);
s_y = zeros(size_glcm_3,1);
cor = zeros(size_glcm_3,1);


for k = 1:size_glcm_3 % number glcms
    
    glcm_sum(k) = sum(sum(cooccur_matrix(:,:,k)));
    glcm(:,:,k) = cooccur_matrix(:,:,k)./glcm_sum(k); % Normalize each glcm
    
    for i = 1:size_glcm_1
        for j = 1:size_glcm_2
                       
            energy(k) = energy(k) + (glcm(i,j,k).^2);
            contrast(k) = contrast(k) + (abs(i - j))^2.*glcm(i,j,k);
            entropy(k) = abs(entropy(k)) + abs((glcm(i,j,k)*log(glcm(i,j,k) + eps)));
            homogeneity(k) = homogeneity(k) + (glcm(i,j,k)/( 1 + abs(i-j) ));
            
            u_x(k) = u_x(k) + (i)*glcm(i,j,k); 
            u_y(k) = u_y(k) + (j)*glcm(i,j,k);
        end
    end
    
    glcm_max(k) = max(max(glcm(:,:,k)));
end


for k = 1:size_glcm_3
    for i = 1:size_glcm_1
        for j = 1:size_glcm_2
            
            s_x(k) = s_x(k) + (((i) - u_x(k))^2)*glcm(i,j,k);
            s_y(k) = s_y(k) + (((j) - u_y(k))^2)*glcm(i,j,k);
            
            auto_correlation(k) = auto_correlation(k) + ((i)*(j)*glcm(i,j,k));
            cor(k) = cor(k) + (((i) - u_x(k))*((j) - u_y(k))*glcm(i,j,k));
            cluster_prominence(k) = cluster_prominence(k) + (((i + j - u_x(k) - u_y(k))^4)*glcm(i,j,k));
            cluster_shade(k) = cluster_shade(k) + (((i + j - u_x(k) - u_y(k))^3)*glcm(i,j,k));
            
        end
    end
    s_x(k) = s_x(k) ^ 0.5;
    s_y(k) = s_y(k) ^ 0.5;
    correlation(k) = cor(k) / (s_x(k)*s_y(k));
end

feature5(1:4) = glcm_max(1:4);

feature5(5:8) = energy(1:4);
feature5(9:12) = contrast(1:4);
feature5(13:16) = entropy(1:4);
feature5(17:20) = homogeneity(1:4);

feature5(21:24) = correlation(1:4);
feature5(25:28) = auto_correlation(1:4);
feature5(29:32) = cluster_prominence(1:4);
feature5(33:36) = cluster_shade(1:4);

clear Offset;
clear gray_image;
clear cooccur_matrix;
clear energy;
clear contrast;
clear entropy;
clear homogeneity;
clear correlation;
clear auto_correlation;
clear cluster_prominence;
clear cluster_shade;
clear size_glcm_1;
clear size_glcm_2;
clear size_glcm_3;
clear glcm;
clear glcm_sum;
clear u_x;
clear u_y;
clear s_x;
clear s_y;
clear cor;
clear k;
clear i;
clear j;

        
        
            
  



