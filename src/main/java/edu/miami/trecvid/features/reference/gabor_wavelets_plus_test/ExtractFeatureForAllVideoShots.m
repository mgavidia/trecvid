%% Generate Feature for all the images

%% Input:
%% modify the following parameter:
%% inputDir: the directory holding all the folds of each video
%% outputFeature: the path for the generated feature file in .csv format, the first column is videoID, the second column is shotID
%% outputErrorShotFrame: the path for the video ID and shot ID corresponding to the shots which do not exist
%% outputDirNotExist: the path the videoID of those videos which are missing
%% num_Of_Features: total dimension of features
%% numOfDir: the maximum ID of the videos, be careful that it might be greater than 
%%           the total number of videos, for example, in TRECVID2011 training data,
%%           total number of videos is 11641, however, the maximum ID of the videos
%%           is 11644.

%% output:
%% outputFeature: the generated feature file in .csv format, the first column is videoID, the second column is shotID
%% outputErrorShotFrame: the video IDs and shot IDs corresponding to the shots which do not exist
%% outputDirNotExist: the videoIDs of those videos which are missing


% house keeping

clear all;
clc;

%% Input dir
% tv11_train
%inputDir = '/nethome/tmeng/HOGFeature/ExtractHogForTV10OriginalSize/Data/keyframeOriginalRawImage/Keyframes_11train';
% tv11_test
%inputDir = '/nethome/tmeng/Keyframes_11test';
% tv12_test
%inputDir = '/nethome/tmeng/TRECVIDData/TRECVID2012_Testing_OriginalSize_keyframes/tv12_keyframes_orig_size';

%outputFeature = '/nethome/dliu4/tv12_feature_extraction/tv12_test_features/gabor_wavelets_plus/TV12Te_gaborPlus108_OS.csv';

%outputErrorShotFrame = '/nethome/dliu4/tv12_feature_extraction/tv12_test_features/gabor_wavelets_plus/TRECVID2012_gabor_wavelets_Error_Shot_Original_Size.csv';

%outputDirNotExist = '/nethome/dliu4/tv12_feature_extraction/tv12_test_features/gabor_wavelets_plus/TRECVID2012_gabor_wavelets_Feature_Dir_Not_Exist_Original_Size.csv';

basedir = '/media/data-share/TRECVID/TRECVID_2014';

basefilename = 'gabor_wavelets_plus'

inputDir = [ basedir '/iacc.2.b.keyframes' ]; % ['/nethome/tmeng/Keyframes_11test'];

outputFeature = [ './' basefilename '.csv' ]; %'/nethome/dliu4/tv12_feature_extraction/tv11_test_features/ycbcr_moment/TRECVID2011_ycbcr_moment_Feature_Test_Original_Size.csv';

outputErrorShotFrame = [ './' basefilename '_error.csv' ]  %'/nethome/dliu4/tv12_feature_extraction/tv11_test_features/ycbcr_moment/TRECVID2011_ycbcr_moment_Error_Shot_Original_Size.csv';

outputDirNotExist = [ './' basefilename '_not_exist.csv' ] 

%% list how many dir are there
%% need to modify the "numOfDir"
%% numOfDir is the largest number of directory number
%numOfDir = 11644;

%% num of features
num_Of_Features = 108;

%% initialize the feature matrix
featureMatrix = [];
errorShotFrame = [];
dirNotExist = [];


for i = 30544:32939%28124:30543

    disp (['processing: ' num2str(i)])

    
    inputDirForI = [num2str(i)];
    currentDirectoryName = fullfile(inputDir,inputDirForI);
    
    %check exist
    if exist(currentDirectoryName)==0
        
        dirNotExist = [dirNotExist;i];
        
        
    end    
     
    
    
    %extract feature
    [currentFeature currentError] = getFeature(currentDirectoryName, i, num_Of_Features);
    
    featureMatrix = [featureMatrix;currentFeature];
    
    if ~isempty(currentError)
       
       errorShotFrame= [errorShotFrame;currentError];
    
    end
    
end

csvwrite(outputFeature,featureMatrix);
csvwrite(outputErrorShotFrame,errorShotFrame);
csvwrite(outputDirNotExist,dirNotExist);
