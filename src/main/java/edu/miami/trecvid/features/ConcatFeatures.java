package edu.miami.trecvid.features;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.NumberFormatException;

import edu.miami.trecvid.utilities.*;

public class ConcatFeatures implements CallInterface {

    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        if (args.length < 3){
            throw new ArgumentException();
        }
        
        try{
            main(args);
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }
    
    // Get Description
    public String description(){
        return "Concat two or more features and output in new file.";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList("features:concat"));
    }
    
    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("/path/to/out.csv", "first-feature", "second-feature", "third or more (optional)"));
    }

	/**
	 * @param args
	 */
    
	public static void main(String[] args) throws IOException{
        String outfile = args[0];

        // First file
        FeatureMap map = new FeatureMap(args[1], true);

        for (int i = 2; i < args.length; i++){
            map.appendFile(args[i], true);
        }

        map.writeToFile(outfile, true);
        return;
    }
}
