package edu.miami.trecvid.features;

import de.lmu.ifi.dbs.jfeaturelib.Progress;
import de.lmu.ifi.dbs.jfeaturelib.features.CEDD;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Comparator;
import java.util.Arrays;
import java.lang.NumberFormatException;

import javax.imageio.ImageIO;

import edu.miami.trecvid.utilities.*;

public class ExtractCedd implements CallInterface {

    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        if (args.length < 3){
            throw new ArgumentException();
        }
        
        try{
            main(args);
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }
    
    // Get Description
    public String description(){
        return "Extracts the CEDD features and creates a csv file of the results.";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList("features:extractcedd"));
    }
    
    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("/path/to/keyframes", "/path/to/out.csv", "/path/to/out.log"));
    }

	/**
	 * @param args
	 */
    
	public static void main(String[] args) throws IOException{
		final Pattern numPattern = Pattern.compile("\\d+");//("shot\\d+_\\d+_RKF_\\d.jpg");//
        final Pattern comparablePattern = Pattern.compile("^.+_(\\d+?)_.+$");

		// Paths to out and log
        String outPath = args[1];		
		String outlog = args[2];
		
        BufferedWriter output = new BufferedWriter(new FileWriter(outPath));
        BufferedWriter log = new BufferedWriter(new FileWriter(outlog));

        // Input path (/path/to/extracted/keyframes)
		String inpath = args[0] + "/";
		File dir = new File(inpath);

		String[] children = dir.list();
		if (children == null) {
	        output.close();	
	        log.close();
		    // Either dir does not exist or is not a directory
		} else {
			
	        int count_all = 0;
            int totalChildren = children.length;
		    for (int i=0; i<children.length; i++) {
		        // Get filename of file or directory
		        String videoFolderName = children[i];
				File folder = new File(inpath+videoFolderName);
				File[] listofFiles = folder.listFiles();
                Arrays.sort(listofFiles, new Comparator<File>(){
                        public int compare(File f1, File f2){
                            Matcher m1 = comparablePattern.matcher(f1.getName());
                            Matcher m2 = comparablePattern.matcher(f2.getName());
                            if (m1.find() && m2.find()){
                                Integer shot1 = Integer.parseInt(m1.group(1));
                                Integer shot2 = Integer.parseInt(m2.group(1));
                                //System.out.printf("shot1: " + shot1 + " shot2: " + shot2);
                                return shot1.compareTo(shot2);
                            }
                            return f1.getName().compareTo(f2.getName());
                        }
                });
				
				String outfileName;
				String fname;
				int catID = i;
				int imgID = 0;
				int kfID = 0;

				for (int k = 0; k<listofFiles.length; k++){
					//if (listofFiles[k].isFile()){
					
						//imgID = k+1;
						//fname = "shot"+Integer.toString(catID)+"_"+Integer.toString(imgID)+"_RKF.jpg";
						
						fname = listofFiles[k].getName();
						outfileName = inpath + videoFolderName + File.separator + fname;
						
						count_all = count_all+1;
						
                        //System.out.println("Processing image " + count_all + ": " + fname);

						Matcher m = numPattern.matcher(fname);
						
						//fname.replaceAll("\\D", "_");
						 
						//String[] sarray = fname.trim().split("_");
						String[] arr = new String[3];
						
						//for (int s = 0; s < sarray.length; s++){
						//	System.out.print(sarray[s]);
						//	
						//}
						int count = 0;
						while (m.find()){
							arr[count] = m.group();
							count = count+1;
							//System.out.println(m.group());
						}
						
                        try {
                            catID = Integer.valueOf(arr[0]);
                            imgID = Integer.valueOf(arr[1]);
                            kfID = Integer.valueOf(arr[2]);
                        } catch (NumberFormatException ex){
                            // Ignore pares of kfID
                        }
						
						log.write(count_all + "," + fname + "," + arr[0] + "," + arr[1] + "," + arr[2] + "\n");
                        
                        
                        //Tools.printCompletionRate(k, listofFiles.length, "Processing File: " + fname);
						
						//int startPos = fname.indexOf("_");
						//int endPos = fname.indexOf(".");
						//catID = Double.valueOf(fname.substring(0, startPos));
						//imgID = Double.valueOf(fname.substring(startPos+1, endPos));							
						
						
						try {
							ColorProcessor cp = new ColorProcessor(ImageIO.read(new File(outfileName)));
					        CEDD cedd = new CEDD();
					        cedd.run(cp);

							output.write(Integer.toString(catID)+",");
							output.write(Integer.toString(imgID)+",");
							output.write(Integer.toString(kfID)+",");		
							
                            double [] data = cedd.getFeatures().get(0);
					        int len = data.length; // size of the cedd feature vector

					        for (int l=0; l<len-1; l++){
						    	output.write(Double.toString(data[l])+",");	        	
					        }
					        output.write(Double.toString(data[len-1])+"\n");						
						}
						catch (RuntimeException ioe){
							log.write(fname);
							continue;
						}
						
		
					//}
				}
                Tools.printCompletionRate(i, children.length, "Processing Folder: " + videoFolderName);
			}
	        output.close();	
	        log.close();
            
            // Output 100%
            Tools.printCompletionRate(totalChildren, totalChildren, "Tasks");
            System.out.println();
	    }						
				
    }
}
