package edu.miami.trecvid.keyframes;

import java.lang.Runtime;
import java.lang.Process;
import java.lang.Thread;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.lang.InterruptedException;
import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import edu.miami.trecvid.utilities.*;

public class ExtractFrames implements CallInterface{
 
    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        //% Extract keyframes with ffmpeg
        if (args.length < 4){
            throw new ArgumentException();
        }
        
        String indexFile = args[0];
        String ffmpegCommand = args[1];
        String srcDir=args[2] + "/";
        String destDir=args[3] + "/";
        
        try {
            int totalLines = Tools.countLines(indexFile);
            Scanner reader = new Scanner(new File(indexFile));
            int count = 0;
            while (reader.hasNextLine()){
                String line = reader.nextLine();
                String fullname = line.split(",")[0];
                String videoid = line.split("_")[0].substring(4);
                String time = line.split(",")[2].substring(0, 8);
                (new File(destDir + videoid)).mkdir();
                /* Slow seek */
                //String ffmpeg = ffmpegCommand + " -i " + srcDir + "YT_" + videoid + ".mp4 -vframes 1 -ss " + time + " " + destDir + videoid + "/" + fullname + ".jpg";
                /* Fast seek */
                String ffmpeg = ffmpegCommand + " -ss " + time + " -i " + srcDir + "YT_" + videoid + ".mp4 -frames:v 1 " + destDir + videoid + "/" + fullname + ".jpg -y";
                //System.out.println("Running: " + ffmpeg);
                Process process = Runtime.getRuntime().exec(ffmpeg);
                int exitVal = process.waitFor();
                Tools.printCompletionRate(count, totalLines, "Processing File: " + fullname);
                //System.out.println("Executed: " + ffmpeg + " with exitval: " + exitVal);
                count++;
            }
            reader.close();
            System.out.println();
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (InterruptedException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
	}
    }
    
    // Get Description
    public String description(){
        return "Extract keyframes with given index file. Note this may be OS dependant and may not work on windows.";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList("keyframe:extract"));
    }
    
    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("index_file", "/dir/to/ffmpeg-exec", "renamed_mp4_src_dir","dest_dir"));
    }
}
