package edu.miami.trecvid.keyframes;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.lang.Double;
import java.io.FilenameFilter;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

import edu.miami.trecvid.utilities.*;

public class GetIndex implements CallInterface{
        
    // Get Description
    public String description(){
        return "Generates the keyframe index from xml files provided by path.";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        List<String> words = new ArrayList<String>();
        words.add("keyframe:getindex");
        return words;
    }
    
    // Get Argument list
    public List<String> arguments(){
        /*List<String> args = new ArrayList<String>();
        args.add("path to xml files");
        return args;*/
        return new ArrayList<String>(Arrays.asList("path_to_mp7_xml_files", "map_keyframe_file_out", "additional_time (optional)"));
    }
    
    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        if (args.length < 2){
            throw new ArgumentException();
        }
        
        String xmlPath = args[0];
        
        String outFile = args[1];
        
        double timeAdded = 0;
        
        if (args.length >= 3){
            timeAdded = Double.valueOf(args[2]);
        }
        
        List<File> files = Tools.getFileList(xmlPath, ".xml");
        
        // Need to sort the list in descending order
        Collections.sort(files);
        
        try {
            File correctionsFile = new File(outFile);
            if (correctionsFile.exists()){
                correctionsFile.delete();
            }
            PrintWriter output = new PrintWriter(new FileWriter(correctionsFile));
        
            //Loop through all video files 
            int count = 0;
            for (File file : files){
                               
                Tools.printCompletionRate(count, files.size(), "Processing File: " + file.getName());
                
                //Read XML file and get Document Object Model node
                DocumentBuilderFactory docbf = DocumentBuilderFactory.newInstance();
                Document document = null;
                DocumentBuilder db = docbf.newDocumentBuilder();
                /* Fix for now because tv13 apparently has an issue closing
                 * <Description> tag in all of iacc.2.A.mp7
                 * Load into a string and add the missing Bracket
                 * /Description> -> </Description>
                 */
                String xml = Tools.fileToString(file);
                if (!xml.contains("</Description>")){
                    // Fix
                    document = db.parse(new InputSource(new StringReader(xml.replace("/Description>", "</Description>"))));
                } else {
                    // It's fine
                    document = db.parse(new InputSource(new StringReader(xml)));
                }
                document.getDocumentElement().normalize();
                
                NodeList videoSegments = document.getElementsByTagName("VideoSegment");
                for (int i=0; i < videoSegments.getLength(); i++){
                    Node node = videoSegments.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE){
                        Element element = (Element) node;
                        String id = element.getAttribute("id");
                        
                        // Check if current VideoSegement is a key frame
                        // Regex match id to .*_RKF$
                        if (id.matches(".*_RKF")){
                            // Get MediaTimePoint element
                            String mediaTimePoint = element.getElementsByTagName("MediaTimePoint").item(0).getTextContent();
                            // Get Timestamp and split hour, minute, seconds and fraction
                            String [] timeStamp = mediaTimePoint.split("(T|:)");
                            String hour = timeStamp[1];
                            String min = timeStamp[2];
                            String sec = String.valueOf((Double.valueOf(timeStamp[3]) + timeAdded));
                            String frac = timeStamp[4];
                            
                            // Split fraction for key frame calculation
                            String [] fraction = frac.split("F");
                            String part = fraction[0];
                            String fps = fraction[1];
                            
                            //Calculate key frame index and time stamp with ffmpeg syntax
                            double frameIndex= (Double.valueOf(hour)*3600 + Double.valueOf(min)*60 + Double.valueOf(sec) + Double.valueOf(part)/Double.valueOf(fps))*Integer.valueOf(fps);
                            String index = String.valueOf(new Double(frameIndex).intValue());
                            String frameTime = String.format("%s:%s:%06.3f", hour, min, Double.valueOf(sec) + (Double.valueOf(part)/Double.valueOf(fps)));
                            output.println(String.format("%s,%s,%s",id, index, frameTime));
                        }
                    }
                }
                count++;
            }
            // Flush
            output.close();
            System.out.println();
        } catch (final SAXException ex){
            System.out.println("Caught SAX Exception Message is: " + ex);
        } catch (final ParserConfigurationException ex){
            System.out.println("Caught ParserConfigurationException Exception Message is: " + ex);
        } catch (final IOException ex){
            System.out.println("Caught IOException Exception Message is: " + ex);
        }
    }
}
