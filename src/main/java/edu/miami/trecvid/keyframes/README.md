Keyframe extraction instruction
===============================

- Step 1: Run the matlab program get_keyframe_index.m to generate mapping file, which indicates the keyframe index and location (timestamp) based on .mp7 files.
		Please specify the .mp7 file path (pathToXMLs) in line 21 and the output file (fid) in line 39. The file map_otherframe_index_2011_test.txt provides an example of the output.
		
- Step 2: Run the script create_otherframes_tv11test.awk to generate keyframes by using the command "awk -f create_keyframes.awk map_keyframe_index.txt",
		where map_keyframe_index.txt is the mapping file generated in Step 1. Please remember to specify the video path and the ffmpeg location in the script.		
