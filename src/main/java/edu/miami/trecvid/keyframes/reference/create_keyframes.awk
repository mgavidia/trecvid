## Program used to create key frame images from mp4 videos
##
## Run environment:
##	1 - folder "mp4_videos": videos should be named YT_zzz.mp4 where zzz is video id
##	2 - input file should have syntax: shot id, frame index, frame time stamp
##	    i.e. shot16_1_RKF,157,00:00:06.280
##
## Program will create folder for each video called TRECVID2011_X where X is video id. 
## All key frame images will be called "shot id".jpg, i.e. shot16_1_RKF.jpg
##
## To run:
##	awk -f create_keyframes.awk map_keyframe_index.txt
## where map_keyframe_index.txt is input file (#2 from run environment above).
##
## Author: Ronald Ocampo

BEGIN { 
	FS=",";
	previousvid=0; 
}

{
	split($1, arr, "_");
	vid=substr(arr[1],5);
	
	if( vid != previousvid )
	{
		system("mkdir /home/phrlm-storage-25/TRECVID/Otherframes_2011_test/TRECVID2011_" vid);
	}

	system("/home/phrlm-storage-14/TRECVID_2009/ffmpeg/ffmpeg-0.5_source/ffmpeg -i /home/phrlm-storage-25/TRECVID/tv11/tv2010_iacc_1_shorts_B/YT_" vid ".mp4 -ss " $3 " /home/phrlm-storage-25/TRECVID/Otherframes_2011_test/TRECVID2011_" vid "/" $1 ".jpg 2> /dev/null");

	print "Finished TRECVID2011_" vid "/" $1 ".jpg";

	previousvid=vid;

}





