% Program used to create mapping of key frame indexes.
% 
% Specify the following:
%       1 - Path to XML files
%       2 - Path to frame rate file
%
% Run code
%
% Syntax:  get_keyframe_index
%
% Output file: map_keyframe_index.txt
% Syntax: shot id, key frame index, key frame time stamp (ffmpeg syntax)
%
% Author:   Ronald Ocampo

function [] = get_keyframe_index()

    %Path to XML files
    %pathToXMLs = 'U:\FDOIStorage\storage15\TRECVID\TRECVID_2011_storage26\mp7_2011_train\';
	%pathToXMLs = '/home/phrlm-storage-25/TRECVID/tv12_keyframes/mp7_error/';
	pathToXMLs = '/home/phrlm-storage-25/TRECVID/mp7_corrected/';
	%Path to frame rate file
%    frameRateFile = 'U:\Ron_PhD\Trecvid2011\Assignment1\Task3\frame_rate_file.txt';
    
    %List of XML files
    %XMLFiles = dir([pathToXMLs '\*.mp7.xml']);
    XMLFiles = dir([pathToXMLs '/*.mp7.xml']);	
    
    %Load frame rate file
%    fid = fopen(frameRateFile);
%    fpsCells = textscan(fid,'%s%f', 'delimiter', ',');
%    fclose(fid);
    %Get cell array of file names
%    fpsFileNames = fpsCells{1};
    %Get vector of fps values
%    fpsValue = fpsCells{2};
%    rows = size(fpsFileNames,1);
    
    fid = fopen('map_keyframe_index_corrected.txt', 'w');
    
    %Loop through all video files 
    for i = 1 : numel(XMLFiles)
                       
        currentXML = XMLFiles(i).name;
		disp(['Processing file ' currentXML]);
		
        %Get current video id
        vid = regexp(currentXML,'\.','split');
        vid = vid{1};
        
        %Loop through all video names to get current fps
%        for k = 1:rows
%            
%            if regexp(fpsFileNames{k},['YT_' vid '\.avi'])
%                currentFPS = fpsValue(k);
%                break;
%            end
%            
%        end

        currentFPS = 0;
        
        %Read XML file and get Document Object Model node
        xDoc = xmlread([pathToXMLs currentXML]);
        allVideoSegItems = xDoc.getElementsByTagName('VideoSegment');
        
        %Loop through all video file tags in XML file
        for j = 0 : allVideoSegItems.getLength - 1
        
            currentVideoSegItem = allVideoSegItems.item(j);
            currentShot = char(currentVideoSegItem.getAttribute('id'));
            
            %Check if current VideoSegment is a key frame
            if regexp(currentShot,'.*_RKF$')
                
                %Get MediaTimePoint element
                thisList = currentVideoSegItem.getElementsByTagName('MediaTimePoint');
                thisElement = thisList.item(0);
                
                %Get timeStamp and split hour, minute, seconds, and
                %fraction
                timeStamp = regexp(char(thisElement.getFirstChild.getData),'(T|:)','split');
                [hour,min,sec,frac] = timeStamp{2:5};
                
                %Split fraction for key frame calculation
                fraction = regexp(frac,'F','split');
                [part,total] = fraction{1:2};
                
                %Calculate key frame index and time stamp with ffmpeg syntax
                frameIndex = int2str((str2double(hour)*3600 + str2double(min)*60 + str2double(sec) + str2double(part)./str2double(total))*currentFPS);
                frameTime = [hour ':' min ':' sec '.' sprintf('%03.0f',(str2double(part)./str2double(total)).*1000)];
                fprintf(fid, '%s\n', [currentShot ',' frameIndex ',' frameTime]);
                
            end
        end
    end
    
    fclose(fid);
    
end
