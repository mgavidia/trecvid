clc;clear;

priority = 4; % change here 1 - 4 ; 4 is the worst run
desc='This run is fourth best.';
saveFile = './final_submission/4th_best_submission.xml';
logFile = './4th_best_submission.log';
flog = fopen(logFile,'w');
% folder that contain raw scores: the first field is score, second is label(usually 0)
% 'test_1_rank.txt'

baseDirectory = '/media/data-share/TRECVID/TRECVID_2013'
%Score_dir = [baseDirectory '/iacc.2.a.submission/version1/bestscore/'];  
Score_dir = './submission/Priority4/'

% load concept_mapping file
mapFile = './concept_mapping.txt';
mapping = csvread(mapFile);

%vskidFile = [baseDirectory '/03_VSKID/VSKID_tv12testing.csv'];
% load VSDID file
%VSKID = csvread(vskidFile);     %'/nethome/qzhu/trecvid/2012/data/VSKID/VSKID_tv12testing.csv');
% Already prepended to results


%%%%%%%%%%% declaration part %%%%%%%%%%%%%%%%%%%%%%
%% videoFeatureExtractionResults' begin and end
% The XML file could only have one videoFeatureExtractionResults begin and
% end
vFEResultsBEGIN='<videoFeatureExtractionResults>';
vFEResultsEND='</videoFeatureExtractionResults>';

%% videoFeatureExtractionRunResult's begin and end
%Within one videoFeatureExtractionResults, it may have many run of
%videoFeatureExtractionRunResult
trType='A';
sysId='FIU-UM-';
classType='M';
locality='N';
targetYear='13';

%%% We submit one run, please change according to actual runs

vFERunResultEND='</videoFeatureExtractionRunResult>';

%% videoFeatureExtractionFeatureResult's begin and end
% Within one videoFeatureExtractionRunResult, it may have as many
% videoFeatureExtractionFeatureResult as the number of cocepts. For 2012,
% it has 346 concepts.
conceptNum = 60;% I will mapping them to 1-500 concept

vFEFResultEND='</videoFeatureExtractionFeatureResult>';

%% item
% Within one videoFeatureExtractionFeatureResult, it may have as many
% items as submission limited. For 2009, the number can be as large as 2000.
%seqNum is from 1 to 2000
maxAllowed=2000; %do not change

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create XML file
fid = fopen(saveFile,'w');
%% begin of XML head and validation DTD
xmlhead='<?xml version="1.0" encoding="UTF-8"?>';
dtd='<!DOCTYPE videoFeatureExtractionResults SYSTEM "http://www-nlpir.nist.gov/projects/tv2013/dtds/videoFeatureExtractionResults.dtd">';
fprintf(fid,'%s\n',xmlhead);
fprintf(fid,'%s\n\n',dtd);

%% begin of videoFeatureExtractionResults
fprintf(fid,'%s\n\n',vFEResultsBEGIN);
% we have up to 6 runs for trecvid2009
  
%% begin of videoFeatureExtractionRunResult (run)

vFERunResultBEGIN=['<videoFeatureExtractionRunResult trType="' trType...  
	'" sysId="' sysId num2str(priority) '" priority="' num2str(priority)...
	'" class="' classType  '" desc="' desc '" targetYear="' targetYear '" loc="' locality...
	'">'];
fprintf(fid,'%s\n\n',vFERunResultBEGIN);
% get the related files
%Score_dir = uigetdir(pwd,['Select your directory for run ' num2str(priority)]) ;

for fnum = 1:conceptNum;
	disp(['--- Processing Concept ' num2str(mapping(fnum))  ' -----'])
    fprintf(flog, '--- Processing Concept %s -----\n', num2str(mapping(fnum)));
	%% start and end of print items
	AllData = getRunData(Score_dir, num2str(mapping(fnum)), flog);
	row = size(AllData,1);
	maxrow = min(row, maxAllowed);

	%% begin of videoFeatureExtractionFeatureResult (concept)
	if(mapping(fnum) < 10)
		fnumdisp=['00' num2str(mapping(fnum))];
	elseif(mapping(fnum) < 100)
		fnumdisp=['0' num2str(mapping(fnum))];
	else
		fnumdisp = num2str(mapping(fnum));
	end
	vFEFResultBEGIN=['<videoFeatureExtractionFeatureResult fNum="' fnumdisp '">'];
	fprintf(fid,'%s\n',vFEFResultBEGIN);

	for seqNum = 1 : maxrow; 
		videoId = AllData(seqNum, 1);
		shotId = AllData(seqNum, 2);
		item=['<item seqNum="' num2str(seqNum) '" shotId="shot' num2str(videoId) '_' num2str(shotId) '"/>'];
		fprintf(fid,'%s\n',item);
	end
	%% end of videoFeatureExtractionFeatureResult (concept)
	fprintf(fid,'%s\n\n',vFEFResultEND);
end
%% end of videoFeatureExtractionRunResult (run)
fprintf(fid,'%s\n\n',vFERunResultEND);

	
%% end of videoFeatureExtractionResults
fprintf(fid,'%s\n',vFEResultsEND);
fclose(fid);
fclose(flog);
clear;
