function sorted_Score = getRunData(directory, fnum, flog)

	if(exist(fullfile(directory, ['test_' num2str(fnum) '_rank.txt']),'file'))
		Data = csvread(fullfile(directory, ['test_' num2str(fnum) '_rank.txt']));
		%Data_w_ID = [VSKID Data];
        %disp(Data_w_ID)
		%sorted_Score = sortrows(Data_w_ID, -3); % sort by score from largest to smallest
        sorted_Score = sortrows(Data, -3); % sort by score from largest to smallest
	else
		fprintf(flog, 'test_%s_rank.txt does not exist.\n', num2str(fnum));
	end
end
