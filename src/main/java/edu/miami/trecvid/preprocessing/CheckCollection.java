package edu.miami.trecvid.preprocessing;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Document;

import edu.miami.trecvid.utilities.*;

public class CheckCollection implements CallInterface{
    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        if (args.length < 1){
            throw new ArgumentException();
        }
        /*
        %this code is for checking collection file and generate the mapping file(.txt and .mat)
        %which contains video id and video name, and find out the dropped and missing videos
        %need to perform before keyframe extraction since it finalizes the # of videos used
        */
        
        String xmlFile = args[0];
        
        try {
            XmlHelper helper = new XmlHelper(xmlFile);

            // %get Elements Tag
            NodeList idItems = helper.getElementsByTagName("id");
            NodeList filenameItems = helper.getElementsByTagName("filename");
            NodeList useItems = helper.getElementsByTagName("use");
            
            List<String> dropped = new ArrayList<String>();
            List<String> missing= new ArrayList<String>();
            Map<String, String> map = new HashMap<String, String>();
            
            File mapfile = new File("./mapC.txt");
            
            if (mapfile.exists()){
                mapfile.delete();
            }
            PrintWriter output = new PrintWriter(new FileWriter(mapfile));
            if (output != null){
                for (int i = 0; i < idItems.getLength(); i++){

                    Node idItem = idItems.item(i);
                    Node filenameItem = filenameItems.item(i);
                    Node useItem = useItems.item(i);

                    Node idchildNode = idItem.getFirstChild();
                    Node filenamechildNode = filenameItem.getFirstChild();
                    Node usechildNode = useItem.getFirstChild();

                    String id = idchildNode.getNodeValue();
                    String filename = filenamechildNode.getNodeValue();
                    String use = usechildNode.getNodeValue();

                    if (id.isEmpty() || filename.isEmpty()){
                        missing.add(id);
                    } else if(use.equals("dropped")){
                        dropped.add(id);
                    } else {
                        output.println(String.format("%s,%s",id,filename));
                        map.put(id ,filename);
                    }
                    
                    Tools.printCompletionRate(i,idItems.getLength(), "Processing file id [" + id + "] of filename [" + filename + "]");
                }
                output.close(); 
                
                if (!dropped.isEmpty()){
                    PrintWriter droppedOut = new PrintWriter(new FileWriter(new File("./droppedVideoFromCollectionC.csv")));
                    droppedOut.println(Tools.join(dropped, ","));
                    droppedOut.close();
                }
                if (!missing.isEmpty()){
                    PrintWriter missingOut = new PrintWriter(new FileWriter(new File("./missingVideoFromCollectionC.csv")));
                    missingOut.println(Tools.join(missing, ","));
                    missingOut.close();
                }
                Tools.saveMapToFile("./mapC.mat", map);
            } else {
                throw new CallableException(getClass().getName(), "can not open map file");
            }
        } catch (XmlHelperException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }
    
    // Get Description
    public String description(){
        return "Checking collection file and generate the mapping file(.txt and .mat).";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList("preprocess:checkcollection"));
    }
    
    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("collections_xml_file"));
    }
}
