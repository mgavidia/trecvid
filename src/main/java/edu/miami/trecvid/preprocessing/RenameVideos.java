package edu.miami.trecvid.preprocessing;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import edu.miami.trecvid.utilities.*;

public class RenameVideos implements CallInterface{
    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        //%rename the videos (from long name to id) according to the mapping file
        if (args.length < 3){
            throw new ArgumentException();
        }
        
        String xmlFile = args[0];
        String srcDir=args[1] + "/";
        String destDir=args[2] + "/";
        
        try {
            XmlHelper helper = new XmlHelper(xmlFile);
            
            //%should match to the mapping file (mapC.txt), order could be differ
            PrintWriter output = new PrintWriter(new FileWriter(new File("./renameVideosLog.txt")));
            
            Map<String,String> map = Tools.loadMapFromFile("./mapC.mat");
            
            List<File> videoFiles = Tools.getFileList(srcDir, ".mp4");
            
            for (int i = 0; i < videoFiles.size(); i++){
                String name = videoFiles.get(i).getName();
                boolean found=false;
                String videoId = "";
                Iterator it = map.entrySet().iterator();
                while (it.hasNext()){
                    Map.Entry pairs = (Map.Entry)it.next();
                    if (name.equals(pairs.getValue())){
                        found = true;
                        videoId = (String)pairs.getKey();
                        break;
                    }
                }
                if (found){
                    Tools.copyFile(new File(srcDir + name), new File(destDir + "YT_" + videoId + ".mp4"));
                    output.println(String.format("%s,%s",videoId,name));
                    Tools.printCompletionRate(i,videoFiles.size(), "Processing File: " + name);
                } else {
                    output.println("file not found: " + name);
                }
            }
            output.close();
        } catch (XmlHelperException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }
    
    // Get Description
    public String description(){
        return "Rename the videos (from long name to id) according to the mapping file.";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList("preprocess:renamevideos"));
    }
    
    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("collections_xml_file", "src_dir","dest_dir"));
    }
}
