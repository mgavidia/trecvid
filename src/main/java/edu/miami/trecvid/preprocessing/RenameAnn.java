package edu.miami.trecvid.preprocessing;

import java.lang.Double;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
//import org.apache.xmlbeans.XmlOptions;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import edu.miami.trecvid.utilities.*;

public class RenameAnn implements CallInterface{
    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        //%rename the videos (from long name to id) according to the mapping file
        if (args.length < 4){
            throw new ArgumentException();
        }
        
        String xlsxFile = args[0];
        String srcDir=args[1] + "/";
        String destDir=args[2] + "/";
        String logFile = args[3];
        
        try {
            InputStream input = new FileInputStream(xlsxFile);

            Workbook wb = WorkbookFactory.create(input);
            Sheet sheet = wb.getSheetAt(0);
            
            PrintWriter output = new PrintWriter(new FileWriter(new File(logFile)));

            for (int i = 1; i <= sheet.getLastRowNum(); i++){
                Row row = sheet.getRow(i);
                Cell conceptName = row.getCell(2);
                Double conceptId = row.getCell(0).getNumericCellValue();
                int id = conceptId.intValue();
                File file = new File(srcDir + conceptName.getStringCellValue() + ".ann");
                boolean skipped = true;
                if (file.exists()){
                    skipped = false;
                    Tools.copyFile(file, new File(destDir + Integer.valueOf(id) + ".ann"));
                }
                if (skipped){
                    output.println("name [" + conceptName + "] id [" + Integer.valueOf(id) + "]");
                }
                Tools.printCompletionRate(i, sheet.getLastRowNum(), "Processing concept [" + conceptName + "] to [" + (skipped ? "Skipped" : Integer.valueOf(id)) + "]");
            }
            output.close();
            System.out.println("\nComplete.");
        } catch (InvalidFormatException ex){
            throw new CallableException(getClass().getName(), ex.getMessage()); 
        } catch (FileNotFoundException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }
    
    // Get Description
    public String description(){
        return "Rename concept names in the ANN files to concept ids according to annotation_statistics_nist.xlsx in the label folder.";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList("preprocess:renameann"));
    }
    
    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("mapping_file_xslx", "src_dir","dest_dir", "missing_log_file"));
    }
}
