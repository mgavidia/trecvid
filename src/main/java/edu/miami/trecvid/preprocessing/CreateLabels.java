package edu.miami.trecvid.preprocessing;

import java.lang.Double;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.lang.Comparable;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import edu.miami.trecvid.utilities.*;

class VideoLabel implements Comparable{
    private String video;
    private String shot;
    private String label;
    
    public VideoLabel(String video, String shot, String label){
        this.video = video;
        this.shot = shot;
        this.label = label;
    }
    
    public String toString(){
        String converted = label;
        if (converted.equals("P")){
            converted = "1";
        } else if (converted.equals("N")){
            converted = "0";
        } else if (converted.equals("S")){
            converted = "-1";
        } else {
            converted = "error";
        }
        return String.format("%s,%s,%s",video,shot,converted);
    }
    
    public int compareTo(Object comparable){
        Integer ours = Integer.parseInt(video);
        Integer theirs = Integer.parseInt(((VideoLabel)comparable).video);
        
        int match = ours.compareTo(theirs);
        if (match == 0){
            Integer ourShot = Integer.parseInt(shot);
            Integer theirShot = Integer.parseInt(((VideoLabel)comparable).shot);
            return ourShot.compareTo(theirShot);
        } 
        
        return match;
    }
    
    public static boolean validLabel(String l){
        return (l.equals("P") || l.equals("N") || l.equals("S"));
    }
}

public class CreateLabels implements CallInterface{
    private void createAll(String renameDir, String allDir) throws ArgumentException, CallableException{
        try{
            for (int i = 1; i <= 500; i++){
                String ann = renameDir + Integer.valueOf(i) + ".ann";
                File annFile = new File(ann);
                if (annFile.exists()){
                    List<VideoLabel> labels = new ArrayList<VideoLabel>();
                    List<VideoLabel> errorLabels = new ArrayList<VideoLabel>();
                    int count = 0, totalLines = Tools.countLines(ann);
                    BufferedReader reader = new BufferedReader(new FileReader(annFile));
                    String line;
                    while ((line = reader.readLine()) != null){
                        String evaluate = line.replaceAll("^.*(/shot.*)$", "$1");
                        String shotID = evaluate.replaceAll(".*/shot(\\d+)_(\\d+)_RKF.*$", "$2");
                        String videoID = evaluate.replaceAll(".*/shot(\\d+)_.*$", "$1");
                        String labelID = evaluate.replaceAll("(.*)([a-z]|[A-Z])$", "$2");
                        VideoLabel label = new VideoLabel(videoID, shotID, labelID);
                        if (VideoLabel.validLabel(labelID)){
                            labels.add(label);
                        } else {
                            errorLabels.add(label);
                        }
                        if (count < totalLines){
                            Tools.printCompletionRate(count, totalLines, "Extracting labels [" + ann + "]");
                        }
                        count++;
                    }
                    reader.close();
                    Collections.sort(labels);
                    Collections.sort(errorLabels);
                    Tools.writeListToFile(labels, allDir + Integer.valueOf(i) + ".label");
                    if (!errorLabels.isEmpty()){
                        Tools.writeListToFile(errorLabels, allDir + Integer.valueOf(i) + ".error");
                    }
                    Tools.printCompletionRate(i, 500, "Processing ann [" + ann + "]");
                }
            }
        } catch (FileNotFoundException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }
    
    private void createTargetConcepts(String xlsxFile, String allDir, String targetDir) throws ArgumentException, CallableException{
        try {
            InputStream input = new FileInputStream(xlsxFile);

            Workbook wb = WorkbookFactory.create(input);
            Sheet sheet = wb.getSheetAt(0);

            for (int i = 0; i < sheet.getLastRowNum(); i++){
                Row row = sheet.getRow(i);
                Cell conceptName = row.getCell(1);
                Double conceptId = row.getCell(0).getNumericCellValue();
                int id = conceptId.intValue();
                File file = new File(allDir + Integer.valueOf(id) + ".label");
                boolean skipped = true;
                if (file.exists()){
                    skipped = false;
                    Tools.copyFile(file, new File(targetDir + Integer.valueOf(id) + ".label"));
                }
                Tools.printCompletionRate(i, sheet.getLastRowNum(), "Processing concept [" + conceptName + "] to [" + (skipped ? "Skipped" : Integer.valueOf(id)) + "]");
            }
        } catch (InvalidFormatException ex){
            throw new CallableException(getClass().getName(), ex.getMessage()); 
        } catch (FileNotFoundException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        } catch (IOException ex){
            throw new CallableException(getClass().getName(), ex.getMessage());
        }
    }
    
    // Run instance
    public void run(String [] args) throws ArgumentException, CallableException{
        /***
         * %generate labels for all the 500 concept
         * % 1 is positive, 0 is negative and -1 is skipped
        */
        if (args.length < 4){
            throw new ArgumentException();
        }
        
        String xlsxFile = args[0];
        String renameDir=args[1] + "/";
        String allDir=args[2] + "/";
        String targetDir=args[3] + "/";
        
        // Generate all 500 concept labels
        createAll(renameDir, allDir);
        //createTargetConcepts(xlsxFile, allDir, targetDir);
        
    }
    
    // Get Description
    public String description(){
        return "Generate labels for all the 500 concepts and then create 346 specifics in target_dir.";
    }
    
    // Get keywords that will invoke this instance
    public List<String> keywords(){
        return new ArrayList<String>(Arrays.asList("preprocess:createlabels"));
    }
    
    // Get Argument list
    public List<String> arguments(){
        return new ArrayList<String>(Arrays.asList("mapping_file_xslx", "ann_rename_dir","all_dir", "target_dir"));
    }
}
