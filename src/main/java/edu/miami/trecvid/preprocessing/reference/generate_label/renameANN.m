%rename concept names in the ANN files to concept ids according to annotation_statistics_nist.xlsx in the label folder

clc;clear all;
ANNDir='/media/data-share/TRECVID/labels/2013_collaborative/ann'
%'C:\research\TRECVID\2012\data\label\tv11_ann';
renameDir='/media/data-share/TRECVID/TRECVID_2013/new_labels/ann'
%C:\research\TRECVID\2012\data\label\tv11_ann_rename';
%[conceptIDList,nameCell]=xlsread('C:\research\TRECVID\2012\data\label\ConceptMapping.xlsx');
[conceptIDList, nameCell]=xlsread('/media/data-share/TRECVID/TRECVID_2013/labels/tv11.sin.500.concepts_ann_v2.xls');
conceptNameCell=nameCell(:,2);

[row,col]=size(conceptNameCell);

for ind=1:row
    conceptName=char(conceptNameCell(ind));
    conceptID=conceptIDList(ind);
    if(exist([ANNDir '\' conceptName '.ann']))
        cmd=['move ' ANNDir '\' conceptName '.ann ' renameDir '\' num2str(conceptID) '.ann'];
        disp(cmd);
        dos(cmd);
    end
end

