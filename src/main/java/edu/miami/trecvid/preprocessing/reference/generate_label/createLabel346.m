%generate labels for 346 concept to be evaluated

clc;clear;

path='C:\research\TRECVID\2012\data\label\tv11_train_label\';
pathNew='C:\research\TRECVID\2012\data\label\tv11_train_label_346\';
[num,txt,raw]=xlsread('C:\research\TRECVID\2012\data\label\annotation_statistics_nist.xlsx');

for i=1:length(num(:,1))
     if(exist([path num2str(num(i,1)) '.label'], 'file'))       
        cmd=['copy ' path num2str(num(i,1)) '.label ' pathNew num2str(i) '.label'];
        disp(cmd);
        dos(cmd);
    end 
end