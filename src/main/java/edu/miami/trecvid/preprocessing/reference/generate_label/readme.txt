1. renameANN.m renames ANN files with concept names in tv11.ann.tgz to concept IDs in ann_rename
(may need to change label name from '-' to '_' in order to match the names in ConceptMapping.xlsx)

2. createLabel.m generates labels for all the ANN files (500 in total for tv11) in ann_rename and 
put in the tv11_train_label

3. createLabel346.m generates labels for 346 the ANN files in ann_rename according to the selected 
346 concepts in annotation_statistics_nist.xlsx 
(download from http://www-nlpir.nist.gov/projects/tv2012/tv11.sin.346.concepts.simple.txt) 
and put in the tv11_train_label

4. genAnnotationStatistics.m counts number of negatives, skipped and positives for each concept