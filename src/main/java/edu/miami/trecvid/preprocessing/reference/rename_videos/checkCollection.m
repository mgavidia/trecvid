%this code is for checking collection file and generate the mapping file(.txt and .mat)
%which contains video id and video name, and find out the dropped and missing videos
%need to perform before keyframe extraction since it finalizes the # of videos used

clc;
clear all;

path='C:\research\TRECVID\2012\data\';
xmlname=[path 'C.collection.xml'];

xDoc = xmlread(xmlname);

%get Elements Tag
idItems = xDoc.getElementsByTagName('id');
filenameItems = xDoc.getElementsByTagName('filename');
useItems = xDoc.getElementsByTagName('use');

dropped=[];
missing=[];
map=[];
fid=fopen([path 'mapC.txt'], 'w');

if(fid)
    for i = 0:idItems.getLength-1

        idItem = idItems.item(i);
        filenameItem=filenameItems.item(i);
        useItem=useItems.item(i);

        idchildNode = idItem.getFirstChild;
        filenamechildNode = filenameItem.getFirstChild;
        usechildNode = useItem.getFirstChild;

        id = char(idchildNode.getData());  
        filename = char(filenamechildNode.getData());
        use = char(usechildNode.getData());

        if (isempty(idchildNode)||(isempty(filenamechildNode)))
            missing = [missing; str2num(id)];
        elseif(strcmp(use,'dropped'))
            dropped = [dropped; str2num(id)];
        else
            fprintf(fid,'%s,%s\n',id,filename);
            temp=struct('id',str2num(id),'filename',filename);
            map=[map temp];         
        end
    end
    fclose(fid); 
    dlmwrite([path 'droppedVideoFromCollectionC.csv'], dropped);
    dlmwrite([path 'missingVideoFromCollectionC.csv'], missing);
    save([path 'mapC.mat'],'map');
else
    disp('can not open map file');
end