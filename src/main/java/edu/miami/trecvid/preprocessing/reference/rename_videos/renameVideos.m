%rename the videos (from long name to id) according to the mapping file

clc;clear;
path='L:\TRECVID\2012\data\';
srcDir='J:\tv12\';
destDir='J:\tv12_rename\';

xmlpathname=[path 'C.collection.xml'];
%should match to the mapping file (mapC.txt), order could be differ
fidout=fopen([path 'renameVideosLog.txt'],'w');
load([path 'mapC.mat']);
mapSize = length(map);
videofiles=dir([srcDir '*.mp4']);
vlen=length(videofiles);
for videoIDX=1:vlen
    vname=videofiles(videoIDX).name;
    %vid=getVideoIdByName(map,vname);
    
    j=1;flag=0; 
    vid=[];
    while(j<=mapSize)
        if (strcmp(vname,map(j).filename))
            flag=1;
            vid=map(j).id;
            %disp(vid);
            break;
        end
            j=j+1;
    end
    
    if(flag==1)
        cmd=['rename ' srcDir vname ' ' 'YT_' num2str(vid) '.mp4'];
        dos(cmd);
        cmd=['move ' srcDir 'YT_' num2str(vid) '.mp4 ' destDir];
        dos(cmd);
        disp(['-------------' num2str(100*videoIDX/vlen) ' % completed ---------------']);
        fprintf(fidout,'%s,%s\n',num2str(vid),vname);
    else
        disp(['file not found: ' videoname]);
    end
end

fclose(fidout);
