%generate labels for all the 500 concept
% 1 is positive, 0 is negative and -1 is skipped

clc;clear;

ANNDir='C:\research\TRECVID\2012\data\label\tv11_ann_rename';
saveDir='C:\research\TRECVID\2012\data\label\tv11_train_label';

for conceptid=1:500
    disp([num2str(conceptid)]);
    ANNfile=[ANNDir '\' num2str(conceptid) '.ann'];
    if(exist(ANNfile,'file'))
       fid=fopen(ANNfile);
       video=[];
       shot=[];
       label=[];
       error=[];
       
       while 1
            tline = fgetl(fid);
            if ~ischar(tline), break, end
            shotInd=strfind(tline,'/shot');
            labelInd=strfind(tline,'_RKF');
            tmp_str=tline(shotInd+5:labelInd-1);
            tmp=strfind(tmp_str,'_');
            videoID=str2num(tmp_str(1:tmp-1));
            shotID=str2num(tmp_str(tmp+1:end));
            label_str=tline(labelInd+5:end);
            if(strcmpi(label_str,'P'))
                video = [video; videoID];
                shot = [shot; shotID];
                label = [label; 1];
            elseif(strcmpi(label_str,'N'))
                video = [video; videoID];
                shot = [shot; shotID];
                label = [label; 0];
            elseif(strcmpi(label_str,'S'))
                video = [video; videoID];
                shot = [shot; shotID];
                label = [label; -1];
            else
                error = [error; videoID shotID];
            end
        end
       fclose(fid);
       dlmwrite([saveDir '\' num2str(conceptid) '.label'],[video shot label]);
       
       if(~isempty(error))
           dlmwrite([saveDir '\' num2str(conceptid) '.error'],error);
       end
       
    end    
end