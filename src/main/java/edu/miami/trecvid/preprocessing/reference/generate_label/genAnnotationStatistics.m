%count number of negatives, skipped and positives for each concept

clc;
clear all;
path='C:\research\TRECVID\2012\data\label\tv11_train_label_346\';
cnt=zeros(346,3);

for i=1:size(cnt,1)
    label=dlmread([path num2str(i) '.label']);
    n=hist(label(:,3),[-1,0,1]);
    cnt(i,:)=[n(2) n(1) n(3)];
end

dlmwrite([path 'annotation_statistics.csv'], cnt);